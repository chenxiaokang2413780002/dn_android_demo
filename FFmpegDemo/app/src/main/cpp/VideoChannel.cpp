//
// Created by 24137 on 2020/2/19.
//

#include "VideoChannel.h"

VideoChannel::VideoChannel(int stream_index, AVCodecContext *pContext) : BaseChannel(stream_index, pContext) {}

VideoChannel::~VideoChannel() {

}

void * task_video_decode(void * pVoid) {
    VideoChannel * videoChannel = static_cast<VideoChannel *>(pVoid);
    videoChannel->video_decode();
    return 0;
}

void * task_video_player(void * pVoid) {
    VideoChannel * videoChannel = static_cast<VideoChannel *>(pVoid);
    videoChannel->video_player();
    return 0;
}

/**
 * 真正的解码，并且，播放
 * 1.解码（解码只有的是原始数据）
 * 2.播放
 */
void VideoChannel::start() {
    isPlaying = 1;

    // 存放未解码的队列开始工作了
    packages.setFlag(1);

    // 存放解码后的队列开始工作了
    frames.setFlag(1);

    // 1.解码的线程
    pthread_create(&pid_video_decode, 0, task_video_decode, this);

    // 2.播放的线程
    pthread_create(&pid_video_player, 0, task_video_player, this);
}

void VideoChannel::stop() {

}

/**
 * 运行在异步线程（视频真正解码函数）
 */
void VideoChannel::video_decode() {
    AVPacket * packet = 0;
    while (isPlaying) {
        // 生产快  消费慢
        // 消费速度比生成速度慢（生成100，只消费10个，这样队列会爆）
        // 内存泄漏点2，解决方案：控制队列大小
        if (isPlaying && frames.queueSize() > 100) {
            // 休眠 等待队列中的数据被消费
            av_usleep(10 * 1000);
            continue;
        }


        int ret = packages.pop(packet);

        // 如果停止播放，跳出循环, 出了循环，就要释放
        if (!isPlaying) {
            break;
        }

        if (!ret) {
            continue;
        }

        // 走到这里，就证明，取到了待解码的视频数据包
        ret = avcodec_send_packet(pContext, packet);
        if (ret) {
            // 失败了（给”解码器上下文“发送Packet失败）
            break;
        }

        // 走到这里，就证明，packet，用完毕了，成功了（给“解码器上下文”发送Packet成功），那么就可以释放packet了
        releaseAVPacket(&packet);

        AVFrame * frame = av_frame_alloc(); // AVFrame 拿到解码后的原始数据包
        ret = avcodec_receive_frame(pContext, frame);
        if (ret == AVERROR(EAGAIN)) {
            // 重来，重新取
            continue;
        } else if(ret != 0) {
            releaseAVFrame(&frame); // 内存释放点
            break;
        }

        // 终于取到了，解码后的视频数据（原始数据）
        frames.push(frame); // 加入队列
    }
    // 出了循环，就要释放
    releaseAVPacket(&packet);
}

/**
 * 运行在异步线程（视频真正播放函数）
 */
void VideoChannel::video_player() {
    // 原始图形宽和高，可以通过解码器拿到
    // 1.TODO 把元素的 yuv  --->  rgba
    SwsContext * sws_ctx = sws_getContext(pContext->width, pContext->height, pContext->pix_fmt, // 原始图形信息相关的
                                          pContext->width, pContext->height, AV_PIX_FMT_RGBA, // 目标 尽量 要和 元素的保持一致
                                          SWS_BILINEAR, NULL, NULL, NULL
    );

    // 2.TODO 给dst_data rgba 这种 申请内存
    uint8_t * dst_data[4];
    int dst_linesize[4];
    AVFrame * frame = 0;

    av_image_alloc(dst_data, dst_linesize,
                   pContext->width, pContext->height, AV_PIX_FMT_RGBA, 1);

    // 3.TODO 原始数据 格式转换的函数 （从队列中，拿到（原始）数据，一帧一帧的转换（rgba），一帧一帧的渲染到屏幕上）
    while(isPlaying) {
        int ret = frames.pop(frame);

        // 如果停止播放，跳出循环, 出了循环，就要释放
        if (!isPlaying) {
            break;
        }

        if (!ret) {
            continue;
        }

        // 格式的转换 (yuv --> rgba)   frame->data == yuv是原始数据      dst_data是rgba格式的数据
        sws_scale(sws_ctx, frame->data,
                  frame->linesize, 0, pContext->height, dst_data, dst_linesize);

        // 渲染，显示在屏幕上了
        // 渲染的两种方式：
        // 渲染一帧图像（宽，高，数据）
        renderCallback(dst_data[0], pContext->width, pContext->height , dst_linesize[0]);
        releaseAVFrame(&frame); // 渲染完了，frame没用了，释放掉
    }
    releaseAVFrame(&frame);
    isPlaying = 0;
    av_freep(&dst_data[0]);
    sws_freeContext(sws_ctx);
}

void VideoChannel::setRenderCallback(RenderCallback renderCallback) {
    this->renderCallback = renderCallback;
}

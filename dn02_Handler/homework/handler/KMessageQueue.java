package com.sunriver.dn_android_demo.handler;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class KMessageQueue {

    BlockingQueue<KMessage> blockingQueue = new ArrayBlockingQueue<>(50);

    public void enqueueMessage(KMessage message){
        try {
            blockingQueue.put(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public KMessage next(){
        try {
            return blockingQueue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}

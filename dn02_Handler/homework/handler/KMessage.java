package com.sunriver.dn_android_demo.handler;


public class KMessage {

    //消息标志
    public int what;
    //消息内容
    public Object obj;
    //Handler对象
    public KHandler target;
    //callback对象
    public Runnable callback;

    public static KMessage obtain() {
        return new KMessage();
    }
}

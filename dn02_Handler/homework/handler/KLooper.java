package com.sunriver.dn_android_demo.handler;

import android.support.annotation.Nullable;

public class KLooper {

    final KMessageQueue mQueue;

    static final ThreadLocal<KLooper> sThreadLocal = new ThreadLocal<KLooper>();

    private KLooper() {
        mQueue = new KMessageQueue();
    }

    public static @Nullable KLooper myLooper() {
        return sThreadLocal.get();
    }

    public static void prepare() {
        if (sThreadLocal.get() != null) {
            throw new RuntimeException("Only one Looper may be created per thread");
        }
        sThreadLocal.set(new KLooper());
    }

    public static void loop(){
        KLooper looper = myLooper();
        KMessageQueue queue = looper.mQueue;

        for (;;){
            KMessage message = queue.next();
            if(message != null){
                message.target.dispatchMessage(message);
            }
        }
    }
}

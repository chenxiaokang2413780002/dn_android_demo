package com.narkang.database_demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.narkang.database_core.BaseDao;
import com.narkang.database_core.BaseDaoFactory;
import com.narkang.database_demo.bean.Person;
import com.narkang.database_demo.bean.User;

public class MainActivity extends AppCompatActivity {

    private BaseDao<User> userDao;
    private BaseDao<Person> personDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userDao = BaseDaoFactory.getInstance().getBaseDao(User.class);
        personDao = BaseDaoFactory.getInstance().getBaseDao(Person.class);
    }

    public void insert(View view) {
        userDao.insert(new User(1, "name1", "pwd1"));

        personDao.insert(new Person(2, "name2", "pwd2"));

        Toast.makeText(this, "执行成功", Toast.LENGTH_SHORT).show();
    }
}

package com.narkang.skin_core;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.narkang.skin_core.utils.SkinThemeUtils;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class SkinLayoutInflaterFactory implements LayoutInflater.Factory2, Observer {

    private static final String[] mClassPrefixList = {
            "android.widget.",
            "android.view.",
            "android.webkit."
    };

    //记录对应View的构造方法
    private static final Map<String, Constructor<? extends View>> mConstructorMap = new HashMap<>();

    private static final Class<?>[] mConstructorSignature = new Class[]{
        Context.class, AttributeSet.class
    };

    private Activity mActivity;
    private SkinAttribute mSkinAttribute;


    public SkinLayoutInflaterFactory(Activity activity, Typeface typeface){
        mActivity = activity;
        mSkinAttribute = new SkinAttribute(typeface);
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {

        View view = createViewFromTag(name, context, attrs);
        if(view == null){
            //自定义View在这里创建
            view = createView(name, context, attrs);
        }

        if(view != null){
            //加载属性
            mSkinAttribute.load(view, attrs);
        }

        return view;
    }

    private View createViewFromTag(String name, Context context, AttributeSet attrs){

        //自定义View
        if(-1 != name.indexOf('.')){
            return null;
        }

        for (int i = 0; i < mClassPrefixList.length; i++) {
            return createView(mClassPrefixList[i] + name, context, attrs);
        }

        return null;
    }

    private View createView(String name, Context context, AttributeSet attrs) {

        Constructor<? extends View> constructor = findConstructor(context, name);

        try {
            return constructor.newInstance(context, attrs);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private Constructor<? extends View> findConstructor(Context context, String name){

        Constructor<? extends View> constructor = mConstructorMap.get(name);

        if(null == constructor){
            try {
                Class<? extends View> clazz = context.getClassLoader().loadClass(name).asSubclass(View.class);
                constructor = clazz.getConstructor(mConstructorSignature);
                mConstructorMap.put(name, constructor);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return constructor;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return null;
    }

    @Override
    public void update(Observable o, Object arg) {
        SkinThemeUtils.updateStatusBarColor(mActivity);
        Typeface typeface = SkinThemeUtils.getSkinTypeface(mActivity);
        mSkinAttribute.setTypeface(typeface);
        mSkinAttribute.applyView();
    }
}

package com.narkang.skin_core;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.text.TextUtils;

import com.narkang.skin_core.utils.SkinPreference;
import com.narkang.skin_core.utils.SkinResources;

import java.lang.reflect.Method;
import java.util.Observable;

public class SkinManager extends Observable {

    private Application mContext;
    private SkinActivityLifecycle mLifecycle;

    private static SkinManager sInstance = null;

    public static SkinManager getInstance() {
        return sInstance;
    }

    private SkinManager(Application context) {
        mContext = context;
        SkinResources.init(context);
        //记录当前皮肤
        SkinPreference.init(context);
        //注册activity生命周期回调
        mLifecycle = new SkinActivityLifecycle();
        context.registerActivityLifecycleCallbacks(mLifecycle);

        loadSkin(SkinPreference.getInstance().getSkin());
    }

    /**
     * 先在Application中初始化
     */
    public static void init(Application context){
        if(sInstance == null){
            synchronized (SkinManager.class){
                if(sInstance == null){
                    sInstance = new SkinManager(context);
                }
            }
        }
    }

    /**
     *  加载皮肤并使用
     */
    public void loadSkin(String skinPath){
        if(TextUtils.isEmpty(skinPath)){
            //使用默认皮肤
            SkinPreference.getInstance().setSkin("");
            //清空资源管理器，恢复默认设置
            SkinResources.getInstance().reset();
        }else {
            try {
                //反射创建AssetManager
                AssetManager assetManager = AssetManager.class.newInstance();
                Method addAssetPath = assetManager.getClass().getMethod("addAssetPath", String.class);
                addAssetPath.invoke(assetManager, skinPath);
                //创建Resource
                Resources appResources = mContext.getResources();
                Resources skinResources = new Resources(assetManager, appResources.getDisplayMetrics(), appResources.getConfiguration());
                //获取皮肤包名
                PackageManager pm = mContext.getPackageManager();
                PackageInfo info = pm.getPackageArchiveInfo(skinPath, PackageManager.GET_ACTIVITIES);
                String pkName = info.packageName;
                SkinResources.getInstance().applySkin(skinResources, pkName);
                //记录皮肤包路径，下次进入使用
                SkinPreference.getInstance().setSkin(skinPath);                
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //通知更新改变皮肤
        setChanged();
        notifyObservers(null);
    }
}

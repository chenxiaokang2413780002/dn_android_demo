package com.narkang.database_core;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.narkang.database_core.annotation.DBField;
import com.narkang.database_core.annotation.DBTable;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BaseDao<T> implements IBaseDao<T> {

    private SQLiteDatabase mSQLiteDatabase;
    private Class<T> mEntityClass;
    private boolean isInit = false;
    private String mTableName;
    private HashMap<String, Field> mCacheMap;

    void init(SQLiteDatabase sqLiteDatabase, Class<T> entityClass) {

        mSQLiteDatabase = sqLiteDatabase;
        mEntityClass = entityClass;

        if (!isInit) {

            //获取表名
            if (entityClass.getAnnotation(DBTable.class) == null) {
                mTableName = entityClass.getSimpleName();
            } else {
                mTableName = entityClass.getAnnotation(DBTable.class).value();
            }

            //执行建表操作
            String createTableSql = getTableSql();
            mSQLiteDatabase.execSQL(createTableSql);

            //缓存操作
            mCacheMap = new HashMap<>();
            initCacheMap();

            isInit = true;
        }
    }

    private void initCacheMap() {
        //1.取得所有字段名
        String sql = "select * from " + mTableName + " limit 1, 0";  //空表
        Cursor cursor = mSQLiteDatabase.rawQuery(sql, null);
        String[] columnNames = cursor.getColumnNames();
        //2.取得所有成员变量
        Field[] declaredFields = mEntityClass.getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
        }
        //3.字段和成员变量一一对应
        for (String columnName : columnNames) {
            Field columnField = null;
            for (Field field : declaredFields) {
                String fieldName;
                if (field.getAnnotation(DBField.class) != null) {
                    fieldName = field.getAnnotation(DBField.class).value();
                } else {
                    fieldName = field.getName();
                }

                if (columnName.equals(fieldName)) {
                    columnField = field;
                    break;
                }
            }

            if (columnField != null) {
                mCacheMap.put(columnName, columnField);
            }
        }
    }

    private String getTableSql() {

        StringBuffer sb = new StringBuffer();
        sb.append("create table if not exists ");
        sb.append(mTableName + "(");

        Field[] declaredFields = mEntityClass.getDeclaredFields();
        for (Field field : declaredFields) {
            Class type = field.getType();

            if (field.getAnnotation(DBField.class) != null) {
                //通过注解获取
                if (type == String.class) {
                    sb.append(field.getAnnotation(DBField.class).value() + " TEXT,");
                } else if (type == Integer.class) {
                    sb.append(field.getAnnotation(DBField.class).value() + " INTEGER,");
                } else if (type == Long.class) {
                    sb.append(field.getAnnotation(DBField.class).value() + " BIGINT,");
                } else if (type == Double.class) {
                    sb.append(field.getAnnotation(DBField.class).value() + " DOUBLE,");
                } else if (type == byte[].class) {
                    sb.append(field.getAnnotation(DBField.class).value() + " BLOG,");
                } else {
                    continue;
                }
            } else {
                //通过注解获取
                if (type == String.class) {
                    sb.append(field.getName() + " TEXT,");
                } else if (type == Integer.class) {
                    sb.append(field.getName() + " INTEGER,");
                } else if (type == Long.class) {
                    sb.append(field.getName() + " BIGINT,");
                } else if (type == Double.class) {
                    sb.append(field.getName() + " DOUBLE,");
                } else if (type == byte[].class) {
                    sb.append(field.getName() + " BLOG,");
                } else {
                    continue;
                }
            }
        }

        if (sb.charAt(sb.length() - 1) == ',') {
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append(")");
        return sb.toString();
    }

    @Override
    public long insert(T entity) {

        //1.准备好ContentValues中需要的数据
        Map<String, String> map = getValues(entity);

        //2.把数据存放到ContentValue中
        ContentValues values = getContentValues(map);

        return mSQLiteDatabase.insert(mTableName, null, values);
    }

    private ContentValues getContentValues(Map<String, String> map) {

        ContentValues values = new ContentValues();
        Set<String> keys = map.keySet();
        Iterator<String> iterator = keys.iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            String value = map.get(key);
            if (value != null) {
                values.put(key, value);
            }
        }

        return values;

    }

    /**
     * 将bean转化为map的形式
     *
     * @param entity
     * @return
     */
    private Map<String, String> getValues(T entity) {
        HashMap<String, String> map = new HashMap<>();
        Iterator<Field> iterator = mCacheMap.values().iterator();
        while (iterator.hasNext()) {
            Field field = iterator.next();
            field.setAccessible(true);

            try {
                Object obj = field.get(entity);
                if (obj == null) {
                    continue;
                }

                String value = obj.toString();

                String key = "";
                if (field.getAnnotation(DBField.class) != null) {
                    key = field.getAnnotation(DBField.class).value();
                } else {
                    key = field.getName();
                }

                if (!TextUtils.isEmpty(key) && !TextUtils.isEmpty(value)) {
                    map.put(key, value);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return map;
    }

    @Override
    public long update(T entity, T where) {

        Map<String, String> values = getValues(entity);
        ContentValues contentValues = getContentValues(values);

        Map<String, String> whereValues = getValues(where);
        Condition condition = new Condition(whereValues);

        return mSQLiteDatabase.update(mTableName, contentValues, condition.whereClause, condition.whereArgs);
    }

    @Override
    public int delete(T where) {

        Map<String, String> values = getValues(where);

        Condition condition = new Condition(values);

        return mSQLiteDatabase.delete(mTableName, condition.whereClause, condition.whereArgs);
    }

    @Override
    public List query(T where) {
        return query(where, null, null, null);
    }

    @Override
    public List query(T where, String orderBy, Integer startIndex, Integer limit) {

        Map<String, String> values = getValues(where);

        String limitString = "";
        if(startIndex != null && limit != null){
            limitString = startIndex + " , " + limit;
        }

        Condition condition = new Condition(values);

        Cursor cursor = mSQLiteDatabase.query(mTableName, null, condition.whereClause, condition.whereArgs, null, orderBy, limitString);

        return getResult(cursor, where);
    }

    private List<T> getResult(Cursor query, T where) {
        ArrayList list = new ArrayList();
        Object item = null;
        while (query.moveToNext()){
            try {
                item = where.getClass().newInstance();  //因为不知道 new  ? , 所以通过反射方式
                //cacheMap  (字段---成员变量的名字)
                Iterator<Map.Entry<String, Field>> iterator = mCacheMap.entrySet().iterator();
                while (iterator.hasNext()){
                    Map.Entry<String, Field> entry = iterator.next();
                    //取列名
                    String columnName = entry.getKey();

                    //以列名拿到列名在游标中的位置
                    int columnIndex = query.getColumnIndex(columnName);

                    Field value = entry.getValue();   //id
                    Class<?> type = value.getType();  //Integer
                    if(columnIndex != -1){  //columnName = "age"
                        if(type==String.class){
                            value.set(item,query.getString(columnIndex));//setid(1)
                        }else if(type==Double.class){
                            value.set(item,query.getDouble(columnIndex));
                        }else if(type==Integer.class){
                            value.set(item,query.getInt(columnIndex));
                        }else if(type==Long.class){
                            value.set(item,query.getLong(columnIndex));
                        }else if(type==byte[].class){
                            value.set(item,query.getBlob(columnIndex));
                        }else{
                            continue;
                        }
                    }
                }
                list.add(item);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        query.close();
        return list;
    }

    /**
     * 条件封装
     */
    class Condition {

        private String whereClause;
        private String[] whereArgs;

        public Condition(Map<String, String> clause) {
            ArrayList list = new ArrayList();
            StringBuffer sb = new StringBuffer();
            sb.append("1=1");
            //取得所有成员变量的名字
            Set<String> keys = clause.keySet();
            Iterator<String> iterator = keys.iterator();
            while (iterator.hasNext()) {
                String key = iterator.next();
                String value = clause.get(key);
                if (value != null) {
                    sb.append(" and " + key + "=?");
                    list.add(value);
                }
            }

            this.whereClause = sb.toString();
            this.whereArgs = (String[]) list.toArray(new String[list.size()]);
        }
    }
}

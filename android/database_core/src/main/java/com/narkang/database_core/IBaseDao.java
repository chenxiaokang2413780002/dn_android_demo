package com.narkang.database_core;

import java.util.List;

/**
 *  数据库操作接口
 */
public interface IBaseDao<T> {

    //插入
    long insert(T entity);

    //更新
    long update(T entity, T where);

    //删除
    int delete(T where);

    List<T> query(T where);

    List<T> query(T where, String orderBy, Integer startIndex, Integer limit);
}

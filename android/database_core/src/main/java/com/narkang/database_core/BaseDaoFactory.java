package com.narkang.database_core;

import android.database.sqlite.SQLiteDatabase;

public class BaseDaoFactory {

    private SQLiteDatabase mSQLiteDatabase;

    private static final BaseDaoFactory ourInstance = new BaseDaoFactory();

    public static BaseDaoFactory getInstance() {
        return ourInstance;
    }

    private BaseDaoFactory() {
        String sqliteDbPath = "data/data/com.narkang.database_demo/kang.db";
        mSQLiteDatabase = SQLiteDatabase.openOrCreateDatabase(sqliteDbPath, null);
    }

    public <T> BaseDao<T> getBaseDao(Class<T> entityClass){
        BaseDao baseDao = null;
        try {
            baseDao = BaseDao.class.newInstance();
            baseDao.init(mSQLiteDatabase, entityClass);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return baseDao;
    }

    public <T extends BaseDao<M>, M> T getBaseDao(Class<T> daoClass, Class<M> entitiyClass){
        T baseDao = null;

        try {
            baseDao = daoClass.newInstance();
            baseDao.init(mSQLiteDatabase, entitiyClass);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        return baseDao;
    }
}

package com.narkang.skin_demo;

import android.app.Application;

import com.narkang.skin_core.SkinManager;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SkinManager.init(this);
    }
}

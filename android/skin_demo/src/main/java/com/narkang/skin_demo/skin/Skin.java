package com.narkang.skin_demo.skin;

import java.io.File;

public class Skin {

    /**
     * 下载地址
     */
    public String url;

    /**
     * 皮肤名
     */
    public String name = "";

    /**
     * 下载完成后缓存地址
     */
    public String path = "";

    public File file;

    public Skin(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public File getSkinFile(File theme) {
        if (null == file) {
            file = new File(theme, name);
        }
        path = file.getAbsolutePath();
        return file;
    }
}

package com.sunriver.dn_android_demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.sunriver.dn_android_demo.handler.KHandler;
import com.sunriver.dn_android_demo.handler.KLooper;
import com.sunriver.dn_android_demo.handler.KMessage;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

    }
}

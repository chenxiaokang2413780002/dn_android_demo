package com.sunriver.dn_android_demo.handler;

import android.util.Log;

public class KHandler {

    final Callback mCallback;
    final KLooper mLooper;
    final KMessageQueue mQueue;

    public KHandler() {
        this(null);
    }

    public KHandler(Callback callback) {

        mLooper = KLooper.myLooper();
        if (mLooper == null) {
            throw new RuntimeException(
                    "Can't create handler inside thread " + Thread.currentThread()
                            + " that has not called Looper.prepare()");
        }
        mQueue = mLooper.mQueue;
        mCallback = callback;
    }

    public interface Callback {
        /**
         * @param msg A {@link android.os.Message Message} object
         * @return True if no further handling is desired
         */
        boolean handleMessage(KMessage msg);
    }

    public void dispatchMessage(KMessage msg) {
        if (msg.callback != null) {
            handleCallback(msg);
        } else {
            if (mCallback != null) {
                if (mCallback.handleMessage(msg)) {
                    return;
                }
            }
            handleMessage(msg);
        }
    }

    private static void handleCallback(KMessage message) {
        message.callback.run();
    }

    public void handleMessage(KMessage msg) {
    }



    public final void sendMessage(KMessage msg) {
        enqueueMessage(mQueue, msg);
    }

    public final void post(Runnable r)
    {
        sendMessage(getPostMessage(r));
    }

    private void enqueueMessage(KMessageQueue queue, KMessage msg) {
        if (queue == null) {
            RuntimeException e = new RuntimeException(
                    this + " enqueueMessage() called with no mQueue");
            Log.w("Looper", e.getMessage(), e);
            return;
        }
        msg.target = this;
        queue.enqueueMessage(msg);
    }

    private static KMessage getPostMessage(Runnable r) {
        KMessage m = KMessage.obtain();
        m.callback = r;
        return m;
    }
}

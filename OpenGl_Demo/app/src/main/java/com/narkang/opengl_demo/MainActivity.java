package com.narkang.opengl_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Bundle;

//看到第96页
public class MainActivity extends AppCompatActivity {

    private GLSurfaceView glSurfaceView;
    private boolean rendererSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);

        glSurfaceView = new GLSurfaceView(this);

        if(checkSupportEs2()){
            glSurfaceView.setEGLContextClientVersion(2);
//            glSurfaceView.setRenderer(new FirstOpenGLRenderer());
//            glSurfaceView.setRenderer(new AirHockeyRenderer(this));
//            glSurfaceView.setRenderer(new AirHockey3DRenderer(this));
            glSurfaceView.setRenderer(new AirHockeyTexturedRenderer(this));
            rendererSet = true;
        }

        setContentView(glSurfaceView);
    }

    //检测是否支持Es2.0
    private boolean checkSupportEs2(){
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo info = activityManager.getDeviceConfigurationInfo();

        return info.reqGlEsVersion >= 0x20000;
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(rendererSet){
            glSurfaceView.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(rendererSet){
            glSurfaceView.onResume();
        }
    }
}

package com.narkang.opengl_demo.util;

import android.util.Log;

import static android.opengl.GLES20.GL_COMPILE_STATUS;
import static android.opengl.GLES20.GL_FRAGMENT_SHADER;
import static android.opengl.GLES20.GL_LINK_STATUS;
import static android.opengl.GLES20.GL_VALIDATE_STATUS;
import static android.opengl.GLES20.GL_VERTEX_SHADER;
import static android.opengl.GLES20.glAttachShader;
import static android.opengl.GLES20.glCompileShader;
import static android.opengl.GLES20.glCreateProgram;
import static android.opengl.GLES20.glCreateShader;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteShader;
import static android.opengl.GLES20.glGetProgramInfoLog;
import static android.opengl.GLES20.glGetProgramiv;
import static android.opengl.GLES20.glGetShaderInfoLog;
import static android.opengl.GLES20.glGetShaderiv;
import static android.opengl.GLES20.glLinkProgram;
import static android.opengl.GLES20.glShaderSource;
import static android.opengl.GLES20.glValidateProgram;

public class ShaderHelper {

    private static final String tag = "ShaderHelper";

    public static int compileVertexShader(String shaderCode){
        return compileShader(GL_VERTEX_SHADER, shaderCode);
    }

    public static int compileFragmentShader(String shaderCode){
        return compileShader(GL_FRAGMENT_SHADER, shaderCode);
    }

    private static int compileShader(int type, String shaderCode){
        //创建新的着色器对象
        int shaderObjectId = glCreateShader(type);
        if(shaderObjectId == 0){
            //创建失败
            Log.e(tag, "Could not create new shader:"+glGetShaderInfoLog(shaderObjectId));
            return 0;
        }
        //把着色器源代码上传到着色器对象里,关联shaderObjectId
        glShaderSource(shaderObjectId, shaderCode);
        //编译着色器
        glCompileShader(shaderObjectId);

        //取出编译状态
        final int[] compileStatus = new int[1];
        glGetShaderiv(shaderObjectId, GL_COMPILE_STATUS, compileStatus, 0);
        //获取可读的日志信息
        Log.e(tag, "Results of compiling source:" + glGetShaderInfoLog(shaderObjectId));

        if(compileStatus[0] == 0){
            glDeleteShader(shaderObjectId);

            return 0;
        }

        return shaderObjectId;
    }

    //链接着色器 将顶点着色器和片段着色器链接在一起变成单个对象
    public static int linkProgram(int vertexShaderId, int fragmentShaderId){

        final int programObjectId = glCreateProgram();

        if(programObjectId == 0){
            glDeleteProgram(programObjectId);
            Log.e(tag, "Results of create program:" + glGetProgramInfoLog(programObjectId));

            return 0;
        }

        //附上着色器
        glAttachShader(programObjectId, vertexShaderId);
        glAttachShader(programObjectId, fragmentShaderId);

        //链接程序
        glLinkProgram(programObjectId);

        int[] linkStatus = new int[1];
        glGetProgramiv(programObjectId, GL_LINK_STATUS, linkStatus, 0);
        if(linkStatus[0] == 0){
            glDeleteProgram(programObjectId);
            Log.e(tag, "Results of link program:" + glGetProgramInfoLog(programObjectId));

            return 0;
        }

        return programObjectId;
    }

    public static boolean validataProgram(int programObjectId){
        glValidateProgram(programObjectId);

        final int[] validateStatus = new int[1];
        glGetProgramiv(programObjectId, GL_VALIDATE_STATUS, validateStatus, 0);
        Log.e(tag, "Results of validating program:" + glGetProgramInfoLog(programObjectId));

        return validateStatus[0] != 0;
    }

    public static int buildProgram(String vertexShaderSource,
                                   String fragmentShaderSource) {
        int program;

        // Compile the shaders.
        int vertexShader = compileVertexShader(vertexShaderSource);
        int fragmentShader = compileFragmentShader(fragmentShaderSource);

        // Link them into a shader program.
        program = linkProgram(vertexShader, fragmentShader);

        validataProgram(program);

        return program;
    }
}

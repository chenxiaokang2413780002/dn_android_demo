package com.narkang.dagger2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.narkang.dagger2.di.Student;
import com.narkang.dagger2.di.Teacher;

import javax.inject.Inject;

public class SecondActivity extends AppCompatActivity {

    @Inject
    Teacher teacher;
    @Inject
    Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

//        MyApp.getComponent().injectActivity(this);

        System.out.println("second-teacher--->>>" + teacher.hashCode());
        System.out.println("second-student---->>>" + student.hashCode());
    }
}

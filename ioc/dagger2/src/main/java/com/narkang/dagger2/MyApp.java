package com.narkang.dagger2;

import android.app.Application;

import com.narkang.dagger2.di.DaggerMainComponent;
import com.narkang.dagger2.di.DaggerPresenterComponent;
import com.narkang.dagger2.di.MainComponent;
import com.narkang.dagger2.di.MainModule;

import java.util.ArrayList;
import java.util.List;

public class MyApp extends Application {

    private static MainComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        //测试不同url
        String url1 = "www.baidu.com";
        String url2 = "www.123.com";
        List list = new ArrayList();
        list.add(url1);
        list.add(url2);

        component = DaggerMainComponent.builder()
                .mainModule(new MainModule(list))
                .presenterComponent(DaggerPresenterComponent.create())
                .build();
    }

    public static MainComponent getComponent() {
        return component;
    }
}

package com.narkang.dagger2.di;

import java.util.List;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@AppScope
@Module
public class MainModule {

    private List<String> urls;

    public MainModule(List<String> urls) {
        this.urls = urls;
    }

    @AppScope
    @Provides
    @Named("url1")
    public String provideUrl1() {
        return urls.get(0);
    }

    @AppScope
    @Provides
    @Named("url2")
    public String provideUrl2() {
        return urls.get(1);
    }

    @AppScope
    @Provides
    public Teacher provideTeacher() {
        return new Teacher();
    }

    @AppScope
    @Provides
    public Student provideStudent(Teacher teacher) {
        return new Student(teacher);
    }

}

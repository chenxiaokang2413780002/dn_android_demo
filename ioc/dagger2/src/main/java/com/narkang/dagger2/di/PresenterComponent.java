package com.narkang.dagger2.di;

import com.narkang.dagger2.MainActivity;
import com.narkang.dagger2.SecondActivity;

import dagger.Component;
import dagger.Subcomponent;

@UserScope
@Component(modules = PresenterModule.class)
public interface PresenterComponent {

    Presenter providePresenter();

}

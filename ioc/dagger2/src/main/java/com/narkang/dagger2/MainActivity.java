package com.narkang.dagger2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.narkang.dagger2.di.DaggerMainComponent;
import com.narkang.dagger2.di.Presenter;
import com.narkang.dagger2.di.Student;
import com.narkang.dagger2.di.Teacher;

import javax.inject.Inject;
import javax.inject.Named;

//https://blog.csdn.net/heng615975867/article/details/80355090
public class MainActivity extends AppCompatActivity {

    @Inject
    Teacher teacher;
    @Inject
    Student student;
    @Inject
    Presenter presenter;
    @Inject
    @Named("url1")
    String url1;
    @Inject
    @Named("url2")
    String url2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyApp.getComponent().injectActivity(this);

        System.out.println("main-teacher--->>>" + teacher.hashCode());
        System.out.println("main-student---->>>" + student.hashCode());
        System.out.println("presenter---->>>" + presenter.hashCode());
        System.out.println("url1---->>>" + url1);
        System.out.println("url2---->>>" + url2);

//        findViewById(R.id.tv).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(MainActivity.this, SecondActivity.class));
//            }
//        });
    }
}

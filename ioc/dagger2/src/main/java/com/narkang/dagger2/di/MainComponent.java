package com.narkang.dagger2.di;

import com.narkang.dagger2.MainActivity;

import dagger.Component;

@AppScope
@Component(modules = {MainModule.class}, dependencies = {PresenterComponent.class})
public interface MainComponent {

    void injectActivity(MainActivity activity);

}

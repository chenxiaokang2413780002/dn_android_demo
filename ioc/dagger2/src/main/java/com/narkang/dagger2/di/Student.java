package com.narkang.dagger2.di;

import javax.inject.Inject;

public class Student {

    public String name;
    public Teacher teacher;

//    @Inject
    public Student(Teacher teacher) {
        this.name = "野猿新一";
        this.teacher = teacher;
    }

    public Student(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("我的名字叫%s啦，我们老师的名字叫%s", name, teacher.name);
    }
}
package com.narkang.dagger2.di;

import dagger.Module;
import dagger.Provides;

@UserScope
@Module
public class PresenterModule {

    @UserScope
    @Provides
    public Presenter providePresenter(){
        return new Presenter();
    }

}

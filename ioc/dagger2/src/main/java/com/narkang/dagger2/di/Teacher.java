package com.narkang.dagger2.di;

import javax.inject.Inject;

public class Teacher {

    public String name;

//    @Inject
    public Teacher() {
        this.name = "苍老湿";
    }

    public Teacher(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("我的名字叫%s啦", name);
    }
}
package com.narkang.ioc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
public @interface EventBase {

    //setOnClickListener
    String listenerSetter();

    //事件类型
    Class<?> listenerType();

    //事件处理
    String callbackMethod();
}

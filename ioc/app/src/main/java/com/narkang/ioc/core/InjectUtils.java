package com.narkang.ioc.core;

import android.content.Context;
import android.view.View;

import com.narkang.ioc.annotation.Bind;
import com.narkang.ioc.annotation.ContentView;
import com.narkang.ioc.annotation.EventBase;
import com.narkang.ioc.proxy.ListenerInvokeHandler;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class InjectUtils {

    /**
     * 注入
     *
     * @param context
     */
    public static void inject(Context context) {

        injectLayout(context);
        injectView(context);
        injectMethod(context);

    }

    /**
     * 方法事件注入
     */
    private static void injectMethod(Context context) {

        Class<?> clazz = context.getClass();
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            Annotation[] annotations = method.getAnnotations();
            for (Annotation annotation : annotations) {
                //判断注解上是否有EventBase标注
                Class<?> annotationType = annotation.annotationType();
                EventBase eventBase = annotationType.getAnnotation(EventBase.class);
                if (eventBase == null) {
                    continue;
                }

                Class<?> listenerType = eventBase.listenerType();
                String listenerSetter = eventBase.listenerSetter();
                String callbackMethod = eventBase.callbackMethod();

                try {
                    Method valueMethod = annotationType.getDeclaredMethod("value");
                    int[] valueId = (int[]) valueMethod.invoke(annotation);
                    for (int value : valueId) {
                        Method findViewById = clazz.getMethod("findViewById", int.class);
                        View view = (View) findViewById.invoke(context, value);
                        if (view == null) {
                            continue;
                        }

                        ListenerInvokeHandler listenerInvokeHandler = new ListenerInvokeHandler(context, method);
                        Object proxy = Proxy.newProxyInstance(listenerType.getClassLoader(), new Class[]{listenerType}, listenerInvokeHandler);
                        Method listenerMethod = view.getClass().getMethod(listenerSetter, listenerType);
                        listenerMethod.invoke(view, proxy);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 控件注入
     *
     * @param context
     */
    private static void injectView(Context context) {
        Class<? extends Context> clazz = context.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            Bind bind = field.getAnnotation(Bind.class);
            if (bind != null) {
                int value = bind.value();
                try {
                    Method method = clazz.getMethod("findViewById", int.class);
                    View view = (View) method.invoke(context, value);
                    field.setAccessible(true);
                    field.set(context, view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 布局注入
     *
     * @param context
     */
    private static void injectLayout(Context context) {
        Class<? extends Context> clazz = context.getClass();
        ContentView contentView = clazz.getAnnotation(ContentView.class);
        if (contentView != null) {
            int layoutId = contentView.value();

            try {
                Method method = clazz.getMethod("setContentView", int.class);
                method.invoke(context, layoutId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}

package com.narkang.ioc.proxy;

import android.content.Context;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ListenerInvokeHandler implements InvocationHandler {

    private Context context;
    private Method activityMethod;

    public ListenerInvokeHandler(Context context, Method activityMethod) {
        this.context = context;
        this.activityMethod = activityMethod;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return activityMethod.invoke(context, args);
    }
}

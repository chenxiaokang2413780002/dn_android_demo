package com.narkang.ioc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.narkang.ioc.annotation.Bind;
import com.narkang.ioc.annotation.ContentView;
import com.narkang.ioc.annotation.OnClick;
import com.narkang.ioc.core.InjectUtils;

@ContentView(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @Bind(R.id.btn1)
    Button btn1;
    @Bind(R.id.btn2)
    Button btn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InjectUtils.inject(this);
    }

    @OnClick({R.id.btn1})
    public void click(View view){
        Toast.makeText(this, "按钮1点击了", Toast.LENGTH_SHORT).show();
    }

    public void btn1(View view) {
        Toast.makeText(this, "--->>>" + btn1.toString(), Toast.LENGTH_SHORT).show();
    }

    public void btn2(View view) {
    }
}

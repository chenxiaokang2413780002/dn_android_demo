#include <iostream>
#include <string>

using namespace std;

/**
 * 类的定义
 */
class Dog {
public:
    char name[20];
    int age;
    int type;

    void speak();
    void run();
};

void Dog::speak() {};
void Dog::run() {};

class Coordinate{

public:
    int x;
    int y;
    void printX(){
        cout << x << endl;
    };
    void printY(){
        cout << y << endl;
    }

};

class Student{
public:
    Student(){
        cout << "Student()" << endl;
    };
    Student(const Student& student){
        cout << "Student(const Student& student)" << endl;
    }
    //析构函数不允许加参数
    ~Student(){
        cout << "~Student" << endl;
    }
    Student(int age, string name): m_strName(name), m_iAge(age){

    }
    void setAge(int _age){
        m_iAge = _age;
    };
    string getName(){
        return m_strName;
    };
private:
    string m_strName;
    int m_iAge;
};

//int main() {
//
//    Student student1;
//    Student student2(student1);
//    Student student3(student2);

//    Student student1(10, "hehe");

//    Student stu;
//    stu.setAge(10);
//    stu.getName();

//    string b = "1";
//    string s = "abf" + b + "asdvfd";

//    Coordinate coordinate;
//    coordinate.x = 10;
//    coordinate.y = 100;
//    coordinate.printX();
//    coordinate.printY();

//    Dog dog;
//    Dog dog2[20];
//
//    dog.age;
//    dog.speak();

//    Dog *dog = new Dog();
//    Dog *dog2 = new Dog[20];
//
//    dog->age = 10;
//    dog->speak();
//
//    for (int i = 0; i < 20; i++) {
//        dog2[i].age = 20;
//        dog2[i].speak();
//    }
//
//    delete dog;
//    delete[]dog2;

//    cout << "Hello, World!" << endl;

//    short int i;
//    short unsigned int j;
//
//    j = 50000;
//    i = j;
//
//    cout << i << " " << j << endl;

//    return 0;
//}

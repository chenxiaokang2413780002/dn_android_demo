cmake_minimum_required(VERSION 3.15)
project(cppStudy)

set(CMAKE_CXX_STANDARD 14)

add_executable(cppStudy main1.cpp Car.h Car.cpp main.cpp auto_ptr_test.cpp unique_ptr_test.cpp demo6_8.cpp demo7_1_Complex.cpp demo7_1_Complex.h demo7_2_io.cpp demo7_2_io.h demo8_3_Singleton.cpp demo8_3_Singleton.h demo4-1.cpp demo4-2.cpp demo5-2.cpp demo6-3.cpp Singleton.cpp Singleton.h Observer.cpp Observer.h Observerable.cpp Observerable.h Demo8-2.cpp Demo8-5.cpp demo8-6.cpp demo8-7.cpp Demo9-3.cpp demo9-10.cpp Demo9-11.cpp)
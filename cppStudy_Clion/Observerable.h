//
// Created by 24137 on 2020/7/19.
//

#ifndef CPPSTUDY_OBSERVERABLE_H
#define CPPSTUDY_OBSERVERABLE_H

#include <list>
#include <string>
#include "Observer.h"

class Observer;

class Observerable {

public:
    Observerable();
    virtual ~Observerable();

    void Attach(Observer* pOb);
    void Detach(Observer* pOb);

    int GetObserverCount() const {
        return _obs.size();
    }

    void DetachAll(){
        _obs.clear();
    }

    virtual void GetSomeNews(string str){
        SetChange(str);
    }

protected:

    void SetChange(string news);

private:
    bool _bChange;
    list<Observer*> _obs;
    void Notify(void* pArg);

};


#endif //CPPSTUDY_OBSERVERABLE_H

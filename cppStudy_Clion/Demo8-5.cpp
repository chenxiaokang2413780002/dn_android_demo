//
// Created by 24137 on 2020/7/19.
//

#include <iostream>
#include <string>

using namespace std;

class LegacyRectangle{
public:
    LegacyRectangle(double x1, double y1, double x2, double y2){
        _x1 = x1;
        _y1 = y1;
        _x2 = y2;
        _y2 = y2;
    }

    void LegacyDraw(){
        cout << "LegacyRectangle:LegacyDraw:" << _x1 << " " << _y1 << " " << _x2 << " " << _y2 << " " <<endl;
    }

private:
    double _x1;
    double _y1;
    double _x2;
    double _y2;
};

class Rectangle{
public:
    virtual void draw(string str) = 0;
};

//第一种适配方式 多继承
class RectangleAdapter: public Rectangle, public LegacyRectangle{
public:
    RectangleAdapter(int x, int y, int w, int h):LegacyRectangle(x, y, x + w, y + h){
        cout << "RectangleAdapter(int x, int y, int w, int h)" << endl;
    }

    virtual void draw(string str){
        cout << "RectangleAdapter:draw" << endl;
        LegacyDraw();
    }
};

//第二种适配方式 组合
class RectangleAdapter2: public Rectangle{

public:
    RectangleAdapter2(int x, int y, int w, int h): _lRect(x, y, x + w, y + h){
        cout << "RectangleAdapter(int x, int y, int w, int h)" << endl;
    }

    virtual void draw(string str){
        cout << "RectangleAdapter:draw" << endl;
        _lRect.LegacyDraw();
    }

private:
    LegacyRectangle _lRect;
};

//int main(){
//
//    int x = 20, y = 50, w = 200, h = 200;
//
//    RectangleAdapter ra(x, y, w, h);
//    Rectangle *pR = &ra;
//
//    pR->draw("Rectangle1");
//
//    cout << endl;
//
//    RectangleAdapter2 ra2(x, y, w, h);
//    Rectangle *pR2 = &ra2;
//
//    pR2->draw("Rectangle2");
//
//    return 0;
//}

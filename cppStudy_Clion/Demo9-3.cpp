//
// Created by 24137 on 2020/7/19.
//

#include <algorithm>
#include <iostream>

using namespace std;

//模板编程
template <class T>
inline bool MySortT(T const &a, T const &b){
    return a>b;
}

template <class T>
inline void DisplayT(T const &a){
    cout << a << " ";
}

//仿函数
struct SortF{

    bool operator()(int a, int b){
        return a > b;
    }

};

struct DisplayF{

    void operator()(int a){
        cout << a << " ";
    }

};

//c++仿函数模板
template <class T>
struct SortTF{

    inline bool operator()(T const &a, T const &b) const {
        return a > b;
    }

};

template <class T>
struct DisplayTF{

    inline void operator()(T const &a) const {
        cout << a << " ";
    }

};

//int main(){
//
//    int arr[] = {4,3,4,1,5};
//
//    //泛型
//    sort(arr, arr + 5, MySortT<int>);
//    for_each(arr, arr+5, DisplayT<int>);
//    cout << endl;
//
//    //仿函数
//    sort(arr, arr + 5, SortF());
//    for_each(arr, arr+5, DisplayF());
//    cout << endl;
//
//    //仿函数模板
//    sort(arr, arr + 5, SortTF<int>());
//    for_each(arr, arr+5, DisplayTF<int>());
//    cout << endl;
//
//    return 0;
//}

//
// Created by 24137 on 2020/6/27.
//

#include "demo7_1_Complex.h"

demo7_1_Complex::demo7_1_Complex() {
    _real = 0;
    _image = 0;
    cout << "demo7_1_Complex()" << endl;
}

demo7_1_Complex::demo7_1_Complex(double r, double i) {
    _real = r;
    _image = i;
    cout << "demo7_1_Complex(double r, double i)" << endl;
}

demo7_1_Complex::~demo7_1_Complex() {
    cout << "~demo7_1_Complex()" << endl;
}

demo7_1_Complex::demo7_1_Complex(const demo7_1_Complex &x) {
    _real = x._real;
    _image = x._image;
    cout << "demo7_1_Complex(const demo7_1_Complex &x)" << endl;
}

demo7_1_Complex demo7_1_Complex::operator+(const demo7_1_Complex &x) {

    demo7_1_Complex tmp;
    tmp._real = _real + x._real;
    tmp._image = _image + x._image;

    cout << "+" << endl;

//    return demo7_1_Complex(_real + x._real, _image + x._image);
    return tmp;
}

demo7_1_Complex& demo7_1_Complex::operator=(const demo7_1_Complex &x) {
    _real = x._real;
    _image= x._image;

    cout << "=" << endl;

    return *this;
}

demo7_1_Complex& demo7_1_Complex::operator++() {     //前置++
    _real++;
    _image++;
    return *this;
}

demo7_1_Complex demo7_1_Complex::operator++(int) {  //后置++
    demo7_1_Complex tmp(*this);
    _real++;
    _image++;
    return tmp;
}

ostream& operator<<(ostream& os, const demo7_1_Complex &x){

    os << "real value is:" << x._real << " image value is:" << x._image;

    return os;

}

istream& operator>>(istream& is, demo7_1_Complex &x){
    is >> x._real >> x._image;
}

//int main(){
//
//    demo7_1_Complex a(1, 2);
//    demo7_1_Complex c(2, 3);
//    demo7_1_Complex b = a + c;
//    demo7_1_Complex d = b;
//
//    cout << a << endl;
//
//    demo7_1_Complex e;
//
//    cin >> e;
//
//    cout << e;

//    demo7_1_Complex complex(1.0, 2.0);
//    cout << complex.real() << endl;
//    cout << complex.image() << endl;
//
//    complex.real(10);
//    complex.image(20);
//
//    cout << complex.real() << endl;
//    cout << complex.image() << endl;
//
//    demo7_1_Complex complex2(30, 40);
//
//    demo7_1_Complex complex3 = complex + complex2;
//    cout << complex3.real() << endl;
//    cout << complex3.image() << endl;
//
//    demo7_1_Complex complex(1.0, 2.0);
//    cout << complex << endl;
//
//    int i;
//    cin >> i;
//    cout << i << endl;

//    return 0;
//}
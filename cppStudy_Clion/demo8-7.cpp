//
// Created by 24137 on 2020/7/19.
//

#include <iostream>

using namespace std;

template<int n>
struct Sum{
    enum Value{N = Sum<n-1>::N + n};
};

template <>
struct Sum<1>{
    enum  Value{ N = 1 };
};

//int main(){
//
//    cout << Sum<100>::N << endl;
//
//    return 0;
//}
//
// Created by 24137 on 2020/7/19.
//

#include "Observerable.h"

class User1: public Observer{

    void Update(void *pArg) {
//        cout << "User1:" << (char *)pArg << endl;
        cout << "User1:" << reinterpret_cast<char *>(pArg) << endl;
    }

};

class User2: public Observer{

    void Update(void *pArg) {
//        cout << "User2:" << (char *)pArg  << endl;
        cout << "User2:" << reinterpret_cast<char *>(pArg)  << endl;
    }

};

class News: public Observerable{

public:

    void GetSomeNews(string str){
        SetChange("News:" + str);
    }


};

//int main(){
//    //帮忙把职责关系分清了
//    User1 u1;
//    User2 u2;
//
//    News n1;
//    n1.GetSomeNews("T0");
//    cout << n1.GetObserverCount() << endl;
//
//    n1.Attach(&u1);
//    n1.Attach(&u2);
//
//    n1.GetSomeNews("T1");
//    cout << n1.GetObserverCount() << endl;
//
//    n1.Detach(&u2);
//    n1.GetSomeNews("T2");
//    cout << n1.GetObserverCount() << endl;
//
//    n1.DetachAll();
//    n1.GetSomeNews("T3");
//    cout << n1.GetObserverCount() << endl;
//
//    return 1;
//}
//
// Created by 24137 on 2020/7/19.
//

#ifndef CPPSTUDY_SINGLETON_H
#define CPPSTUDY_SINGLETON_H

#include <iostream>

using namespace std;

class Singleton {

public:
    static const Singleton* getInstance();
    static void doSomething(){
        cout << "doSomething" <<endl;
    }

private:
    Singleton();
    ~Singleton();

    static Singleton* This; //使用静态变量解决资源的分配和释放
};


#endif //CPPSTUDY_SINGLETON_H

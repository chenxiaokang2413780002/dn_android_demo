//
// Created by 24137 on 2020/7/19.
//

#include "Observerable.h"

Observerable::Observerable():_bChange(false){

}

Observerable::~Observerable() {

}

void Observerable::Attach(Observer* pOb){

    if(pOb == NULL){
        return;
    }

    //auto c++11
    auto it = _obs.begin();
    for(; it != _obs.end(); it++){
        if(*it == pOb){
            return;
        }
    }

    _obs.push_back(pOb);

}

void Observerable::Detach(Observer* pOb){

    if((pOb == NULL) || (_obs.empty() == true)){
        return;
    }

    _obs.remove(pOb);
}

void Observerable::SetChange(string news){

    _bChange = true;
    Notify((void *) news.c_str());

}

void Observerable::Notify(void* pArg){
    if(_bChange == false){
        return;
    } else{
        auto it = _obs.begin();
        for(; it != _obs.end(); it++){
            (*it) -> Update(pArg);
        }

        _bChange = false;
    }
}

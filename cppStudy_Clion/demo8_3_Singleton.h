//
// Created by 24137 on 2020/7/4.
//

#ifndef CPPSTUDY_DEMO8_3_SINGLETON_H
#define CPPSTUDY_DEMO8_3_SINGLETON_H

#include <iostream>

using namespace std;

class demo8_3_Singleton {

public:
    static const demo8_3_Singleton* getInstance();
    static void DoSomething(){
        cout << "DoSomething" << endl;
    }

private:
    demo8_3_Singleton();
    ~demo8_3_Singleton();

    static demo8_3_Singleton* This;   //static 修饰的区域为全局区域   使用静态变量帮助解决资源的分配和释放

};


#endif //CPPSTUDY_DEMO8_3_SINGLETON_H

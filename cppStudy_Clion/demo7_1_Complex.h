//
// Created by 24137 on 2020/6/27.
//

#ifndef CPPSTUDY_DEMO7_1_COMPLEX_H
#define CPPSTUDY_DEMO7_1_COMPLEX_H

#include <iostream>
using namespace std;

class demo7_1_Complex {

public:
    demo7_1_Complex();  //构造函数 实体创建的时候自动调用
    demo7_1_Complex(double r, double i);//构造函数
    virtual ~demo7_1_Complex();   //析构函数 实体销毁时候自动调用
    demo7_1_Complex(const demo7_1_Complex &x); //拷贝构造函数

    //get set
    double real() const { return _real; }

    void real(double d) { _real = d; }

    void image(double i) { _image = i; }

    double image() const { return _image; }

    //运算符重载
    demo7_1_Complex operator+(const demo7_1_Complex &x);  //+
    demo7_1_Complex& operator=(const demo7_1_Complex &x);  //=

    demo7_1_Complex &operator++();    //前置++
    demo7_1_Complex operator++(int);  //后置++

//protected:
//friend

    friend ostream& operator<<(ostream& os, const demo7_1_Complex &x);
    friend istream& operator>>(istream& is, demo7_1_Complex &x);

private:
    double _real;
    double _image;

};


#endif //CPPSTUDY_DEMO7_1_COMPLEX_H

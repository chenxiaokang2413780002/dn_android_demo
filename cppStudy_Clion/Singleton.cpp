//
// Created by 24137 on 2020/7/19.
//

#include "Singleton.h"

//Singleton* Singleton::This = NULL;  //懒汉
Singleton* Singleton::This = new Singleton(); //饿汉

const Singleton* Singleton::getInstance(){
    if(!This){
        This = new Singleton();
    }

    return This;
}

Singleton::Singleton() {

}

Singleton::~Singleton() {

}

//int main(){
//
//    Singleton::getInstance() -> doSomething();
//
//
//
//}
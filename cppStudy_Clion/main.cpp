//
// Created by zztzt on 2019/12/20.
//

#include <iostream>

class Array {
public:
    Array() {
        m_iCount = 5;
        m_pArr = new int[m_iCount];
    };

    Array(const Array &arr) {
        m_iCount = arr.m_iCount;
        m_pArr = new int[m_iCount];
        for (int i = 0; i < m_iCount; ++i) {
            m_pArr[i] = arr.m_pArr[i];
        }
    };

private:
    int m_iCount;
    int *m_pArr;
};

//int main() {
//
//    Array array1;
//    Array array2 = array1;
//
//    return 0;
//}
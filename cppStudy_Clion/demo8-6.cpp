//
// Created by 24137 on 2020/7/19.
//

#include <iostream>
#include <string.h>

using namespace std;

template <class T>
T maxValue(T a, T b){
    cout << "maxValue(T a, T b)" << endl;
    return a > b? a:b;
}

//特化处理
template<>
char* maxValue(char *a, char *b){
    cout << "maxValue(char *a, char *b)" << endl;
    return (strcmp(a, b) > 0?(a):(b));
}

template<class T1, class T2>
int maxValue(T1 a, T2 b){
    return static_cast<int>(a > b? a: b);
}

//int main(){
//
//    cout << maxValue(1, 2) << endl;
//    cout << maxValue(1.0, 2.9) << endl;
//
//    char* s1 = "hello";
//    char* s2 = "world";
//
//    cout << maxValue(s1, s2) << endl;
//
//    cout << maxValue(1, '2') << endl;
//
//    return 0;
//}
//
// Created by 24137 on 2020/7/19.
//

#ifndef CPPSTUDY_OBSERVER_H
#define CPPSTUDY_OBSERVER_H

#include <iostream>

using namespace std;

class Observer {

public:
    Observer() { ; }
    virtual ~Observer() { ; }

    //当被观察者发生变化时，通知
    virtual void Update(void* pArg) = 0;

};


#endif //CPPSTUDY_OBSERVER_H

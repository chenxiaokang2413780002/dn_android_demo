//
// Created by zztzt on 2020/6/2.
//

#include <iostream>
#include <memory>

using namespace std;

//int main(){
//
//    auto w = make_unique<int>(10);
//    cout << *(w.get()) << endl;
//
////    auto w2 = w;
//
//    auto w2 = move(w);  //通过move进行控制权转移
//    cout << ((w.get() != nullptr)? (*w.get()): -1) << endl;
//    cout << ((w2.get() != nullptr)? (*w2.get()): -1) << endl;
//
//    return 0;
//}
//
// Created by 24137 on 2020/7/19.
//
#include <thread>
#include <iostream>
#include <mutex>

using namespace std;

//取钱
void Withdraw(mutex& m, int& money){
    for(int index = 0; index<100; index++){
        m.lock();
        money += 1;
        m.unlock();
    }
}

//存钱
void Deposit(mutex& m, int& money){
    for(int index = 0; index<100; index++){
        m.lock();
        money -= 2;
        m.unlock();
    }
}

int main(){

    int money = 2000;
    mutex m;

    cout << "current money is:" << money << endl;

    //thread中传引用用ref
    thread t1(Deposit, ref(m), ref(money));
    thread t2(Withdraw, ref(m), ref(money));

    t1.join();
    t2.join();

    cout << "Finally money is:" << money << endl;

    return 0;
}

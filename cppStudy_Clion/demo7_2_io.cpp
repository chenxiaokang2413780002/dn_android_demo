//
// Created by 24137 on 2020/6/27.
//

#include "demo7_2_io.h"

//文件拷贝

static const int bufferLen = 2048;

bool CopyFile(const string& str, const string& dst){
    ifstream in(str.c_str(), std::ios::in | std::ios::binary);
    ofstream out(dst.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);

    if(!in || !out){
        return false;
    }

    char temp[bufferLen];
    while (!in.eof()){
        in.read(temp, bufferLen);
        streamsize  count = in.gcount();
        out.write(temp, count);
    }

    in.close();
    out.close();

    return true;
}

//int main() {
//
////    cout << CopyFile("testBuffer.txt","testBuffer2.txt") << endl;
//
//    char a;
//    int index = 0;
//    fstream fout;
//    fout.open("testBuffer.txt", ios::app);
//    if(fout.fail()){
//        cout << "The file open fail" << endl;
//    }
//
//    while (cin >> a) {
//
////        cout << "in while：" << a << endl;
//        fout << "in while: " << a << endl;
//        index++;
//        if (index == 5) {
//            break;
//        }
//
//    }
//
//    cin.ignore(numeric_limits<streamsize>::max(), '\n');  //清空缓冲区脏数据
//
//    char ch;
//    cin >> ch;
////    cout << "out while：" << ch << endl;
//    fout << "out while：" << ch << endl;
//    fout.close();
//
//    return 0;
//}

//int main()
//{
//    char data[200];
//    fstream  afile;
//    afile.open("afile.dat", ios::app|ios::out|ios::in);
//    cin>>data;
//    //将数据写入文件
//    afile<<data;
//    //将数据从文件中读取出来
//    afile>>data;
//    cout<<data;
//    afile.close();
//    return 0;
//}

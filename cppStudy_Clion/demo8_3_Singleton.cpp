//
// Created by 24137 on 2020/7/4.
//

#include "demo8_3_Singleton.h"

demo8_3_Singleton* demo8_3_Singleton::This = NULL;  //懒汉
//demo8_3_Singleton* demo8_3_Singleton::This = new demo8_3_Singleton();  //饿汉

const demo8_3_Singleton* demo8_3_Singleton::getInstance(){

    if(!This){
        This = new demo8_3_Singleton();
    }
    return This;

}

demo8_3_Singleton::demo8_3_Singleton() {

}

demo8_3_Singleton::~demo8_3_Singleton() {

}


//int main(){
//
//    demo8_3_Singleton::getInstance()->DoSomething();
//
//    return 0;
//}
//
// Created by 24137 on 2020/7/15.
//

#include <iostream>

using namespace std;

int main2(){

    //注意指针在定义和间接访问的区别
//    int i = 4;  int *iP = &i;  cout << (*iP) << endl;
//    double d = 3.14;  double * dP = &d;  cout << (*dP) << endl;
//    char c = 'a';  char *cP = &c;  cout << (*cP) << endl;

    int c[4] = {1, 2, 3, 4};
    int * a[4];
    int (*b)[4];  
    b = &c;

    for(int i = 0; i<4; i++){
        a[i] = &(c[i]);
    }

    cout << *(a[0]) << endl;
    cout << (*b)[0] << endl;

    return 0;
}
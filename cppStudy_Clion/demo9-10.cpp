//
// Created by 24137 on 2020/7/19.
//

#include <thread>
#include <iostream>
#include <mutex>

using namespace std;

mutex g_mutex;

void T1(){
    g_mutex.lock();
    cout << "Hello" << endl;
    g_mutex.unlock();
}

void T2(const char* str){
    g_mutex.lock();
    cout << str << endl;
    g_mutex.unlock();
}

//int main(){
//
//    thread t1(T1);
//    thread t2(T2, "Hello world");
//    t1.join();
//    t2.join();
//
//    return 0;
//
//}
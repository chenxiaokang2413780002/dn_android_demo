//
// Created by zztzt on 2019/12/19.
//

#ifndef CPPSTUDY_CAR_H
#define CPPSTUDY_CAR_H

class Car{
public:
    void run();
    void stop();
    void changeSpeed();
};

#endif //CPPSTUDY_CAR_H


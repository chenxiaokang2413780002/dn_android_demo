package com.narkang.cppstudy;

public class ExceptionTest {

    /**
     * Native 调用  Java 方式时，导致异常了并不会立即终止 Native 方法的执行
     *
     * @throws NullPointerException
     */
    private void callback() throws NullPointerException {
//        throw new NullPointerException("CatchThrow.callback by Native Code");
    }
}

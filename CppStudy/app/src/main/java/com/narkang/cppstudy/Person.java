package com.narkang.cppstudy;

import android.util.Log;

public class Person {

    private static final String TAG = Person.class.getSimpleName();

    public void setStudent(Student student) {
        Log.d(TAG, "setStudent: " + student.toString());
    }

}
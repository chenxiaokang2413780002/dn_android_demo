package com.narkang.cppstudy;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

//https://blog.csdn.net/k346k346/article/details/81478223
public class MainActivity extends AppCompatActivity {

    private TextView tv;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        tv = findViewById(R.id.sample_text);
        tv.setText(stringFromJNI());

        findViewById(R.id.btn_testStringAndInt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] ints = new int[]{1,2,3,4,5,6};
                String[] strs = new String[]{"李小龙","李连杰","李元霸"};
                testStringAndInt(10, "哈哈", ints, strs);
            }
        });
        findViewById(R.id.btn_testStudentUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testUpdateStudent(new Student());
            }
        });
        findViewById(R.id.btn_testStudentInPeopleUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testStudentInPeopleUpdate();
            }
        });
        findViewById(R.id.btn_testConstructionMethod).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testConstructionMethod();
            }
        });
        findViewById(R.id.btn_testUnDog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testUnDog();
            }
        });
        findViewById(R.id.btn_testDynamicRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testDynamicRegisterFromJava01();
            }
        });
        findViewById(R.id.btn_testDynamicRegister02).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testDynamicRegisterFromJava02();
            }
        });
        findViewById(R.id.btn_testActivityUiUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testActivityUiUpdateJava();
            }
        });
        findViewById(R.id.btn_testAutoPtr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testAutoPtr();
            }
        });
        findViewById(R.id.btn_testException).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    testException();
                }catch (IllegalArgumentException e){
                    Log.e("narkang", e.getMessage());
                }

                try {
                    doit();
                } catch (Exception e) {
                    Log.e("narkang", "In Java " + e.getMessage());
                }
            }
        });
    }

    private void updateTextUI(){
        if(Looper.getMainLooper() == Looper.myLooper()){
            tv.setText("通过native调用，在主线程中更新UI_01");
        }else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tv.setText("通过native调用，在主线程中更新UI_02");
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unThreadJava();
    }

    public native String stringFromJNI();

    //测试string和int
    public native void testStringAndInt(int count, String text, int[] countArray, String[] textArray);
    //测试通过native方法更新java中的自定义对象
    public native void testUpdateStudent(Student student);
    //测试通过native方法修改Person中的方法，方法中的参数为Student
    public native void testStudentInPeopleUpdate();
    //测试通过native方法去实例化指定构造方法的对象
    public native void testConstructionMethod();
    //测试手动释放对象
    public native void testUnDog();
    //测试动态注册
    public native void testDynamicRegisterFromJava01();
    public native void testDynamicRegisterFromJava02();
    //测试线程
    public native int testActivityUiUpdateJava();
    //手动释放native对象内存
    private native void unThreadJava();
    //测试智能指针autoptr
    private native void testAutoPtr();
    //测试异常处理
    private native void testException() throws IllegalArgumentException;

    private native void doit() throws IllegalArgumentException;
}

//
// Created by 24137 on 2019/12/22.
//
#include <iostream>
#include <string>
#include "main.h"
#include "mylog.h"
#include "Student.h"
#include "MyInstance.h"

//对象的两种创建方式
void test(){

    //栈中创建对象
    Student student;
    //堆中创建对象
    Student *student1 = new Student(23, "ruby", "男");
    student1 -> setAge(24);

    //释放内存
    delete student1;
    student1 = nullptr;

}

//友元函数
void updateStudent(Student *student){

    student -> m_iAge = 17;
    student -> m_strSex = "女";

}

//特殊的操作符重载
//void * operator new (size_t size){
//    LOG("有人new我了，大小是:%d", size);
//    return malloc(size);
//}
//
//void operator delete(void *p){
//    LOG("有人delete我了，内存地址是:%#x", p);
//}

//操作符重载
class Operator{
public:
    int count;
    int64_t count2;

public:
    //进行操作符重载
    Operator operator * (const Operator& o){
        Operator temp;
        temp.count = this -> count * o.count;
        return temp;
    }
};

int main(){

//    test();

//    Student student;
//    updateStudent(&student);

//    操作符重载
//      Operator anOperator1;
//      anOperator1.count = 100;
//
//      Operator anOperator2;
//      anOperator2.count = 3;
//
//      Operator anOperator3;
//      anOperator3 = anOperator1 * anOperator2;
//      LOG("anOperator3:%d", anOperator3.count);

//单例对象
//    MyInstance *instance = MyInstance::getInstance();
//    instance -> show();
//    instance -> show2();

//    Student *student = new Student;
//    delete student;
//    student = nullptr;
}
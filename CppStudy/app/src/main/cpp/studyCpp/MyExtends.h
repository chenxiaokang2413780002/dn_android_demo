//
// Created by zztzt on 2019/12/24.
//

#ifndef CPPSTUDY_MYEXTENDS_H
#define CPPSTUDY_MYEXTENDS_H

#endif //CPPSTUDY_MYEXTENDS_H

#include <iostream>
#include <string>
#include "mylog.h"

using namespace std;

class Person{
private:
    int age;
    char sex;
public:

    string name = "Kevin";

    void show01() {
        LOG("fuShow run ...");
    }

    void show02() {
        LOG("fuShow2 run ...");
    }

    void show03() {
        LOG("fuShow3 run ...");
    }

    // 虚函数
    virtual void show04() {
        LOG("fuShow4 run ...");
    }

    // 纯虚函数 == Java的抽象方法
    virtual void show05() = 0;
};

class Person2{

};

class Person3{

};

//c++多继承  公有继承
class Workder: public Person, Person2, Person3{
public:
    void show01() {
        LOG("ziShow run ...");
    }

    void show02() {
        LOG("ziShow2 run ...");
    }

    void show03() {
        LOG("ziShow3 run ...");
    }

    void show04() {
        LOG("ziShow4 run ...");
    }

    void show05() {
        LOG("ziShow5 run ...");
    }
};
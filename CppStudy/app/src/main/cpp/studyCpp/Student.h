//
// Created by 24137 on 2019/12/22.
//
#include <iostream>
#include <string>

#ifndef CPPSTUDY_STUDENT_H
#define CPPSTUDY_STUDENT_H
#endif //CPPSTUDY_STUDENT_H

using namespace std;

class Student {

    string m_strNumber;  //默认是不可访问的 private

public:
    Student();  //无参构造方法
    Student(int age, string name, string sex); //有参构造
    ~Student(); // 析构函数
    void setAge(int age);
    int getAge();
    string getSex();
    string getName();
    //友元函数
    friend void updateStudent(Student *student);
    //友元类
    friend class Teacher;

private:
    int m_iAge;
    string m_strName;
    string m_strSex;

};

class Teacher{

    void updateStudent(Student *student){
        student -> m_iAge = 12;
    }

};




//
// Created by zztzt on 2019/12/26.
//

#include <iostream>
#include <stack>
#include "mylog.h"
#include "testStack.h"

using namespace std;

void main_testStack(){

    stack<int> stack1;

    //增
    stack1.push(1);
    stack1.push(2);
    stack1.push(3);

    //删
    stack1.pop();

    //查
    int top = stack1.top();
    LOG("栈顶部元素是：%d", top);
}
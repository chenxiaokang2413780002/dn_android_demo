//
// Created by zztzt on 2019/12/26.
//
#include <iostream>
#include <string>
#include <map>
#include "mylog.h"
#include "testMap.h"

using namespace std;

void main_testMap() {
    map<int, char> map1;

    //增
    map1.insert(pair<int, char>(1, 'c'));
    map1.insert(pair<int, char>(2, 'g'));
    map1.insert(pair<int, char>(3, 'd'));

    //删
    map1.erase(1);

    //改
    map1.insert(pair<int, char>(1, 'e'));

    //查
    map<int, char>::iterator iterator2 = map1.find(1);
    LOG("第一个元素key=%d, value=%c", iterator2->first, iterator2->second);

    //遍历
    map<int, char>::iterator iteratorBegin = map1.begin();
    map<int, char>::iterator iteratorEnd = map1.end();
    for (; iteratorBegin != iteratorEnd; iteratorBegin++) {
        LOG("遍历的元素key=%d, value=%c", iteratorBegin->first, iteratorBegin->second);
    }
}
//
// Created by 24137 on 2019/12/29.
// 多线程
//
//#include <iostream>
#include <string>
#include <pthread.h>
#include <unistd.h>
#include "testThread.h"
#include "mylog.h"

using namespace std;

//自定义一个函数，异步任务使用
void *customPThreadMethod(void *value) {

    int tempValue = *static_cast<int *>(value);
//    int tempValue = *((int *)value);

    LOG("accept value from out method is %d", tempValue);
    for (int i = 0; i < 10; ++i) {
        sleep(1);
        LOG("customPThreadMethod:%d", i + 1);
    }

    return 0;  //必须返回一个值，不然程序会闪退
}

void main_testThread() {

    //线程基本使用
//    int pthread_create(pthread_t *__pthread_ptr, pthread_attr_t const *__attr,
//                       void *(*__start_routine)(void *), void *);
//    pthread_t pThreadID; //函数指针
//    int value = 1000;
//    pthread_create(&pThreadID, 0, customPThreadMethod, &value);
//
//    pthread_join(pThreadID, 0);  //会等待线程执行完毕之后，才会执行后面的代码
//
//    LOG("after thread");

    pthread_t pThreadID;  //线程ID，允许有野指针
    pthread_attr_t pThreadAttr; //线程属性，不允许有野指针 有坑

    //初始化：线程属性
    pthread_attr_init(&pThreadAttr);

    //开启分离线程，pthread_join就失效了
//    pthread_attr_setdetachstate(&pThreadAttr, PTHREAD_CREATE_DETACHED);
    //开启非分离线程和pthread_join关联起来了
    pthread_attr_setdetachstate(&pThreadAttr, PTHREAD_CREATE_JOINABLE);

    int value = 1000;
    pthread_create(&pThreadID, &pThreadAttr, customPThreadMethod, &value);

    //会等待线程执行完毕之后，才会执行后面的代码
    pthread_join(pThreadID, 0);

    //线程属性，进行回收
    pthread_attr_destroy(&pThreadAttr);

    LOG("after thread");
}
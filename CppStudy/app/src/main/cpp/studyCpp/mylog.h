//
// Created by 24137 on 2019/12/22.
//

#ifndef CPPSTUDY_MYLOG_H
#define CPPSTUDY_MYLOG_H

#endif //CPPSTUDY_MYLOG_H

// 使用NDK里面的日志库来打印  liblog.so
#include <android/log.h>

#define TAG "ruby"

#define LOG(...)__android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)
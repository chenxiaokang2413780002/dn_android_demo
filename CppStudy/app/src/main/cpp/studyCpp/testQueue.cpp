//
// Created by zztzt on 2019/12/26.
//
#include <iostream>
#include <queue>
#include "mylog.h"
#include "testQueue.h"

using namespace std;

void main_testQueue(){

    queue<int> queue1;

    //增
    queue1.push(1);
    queue1.push(2);
    queue1.push(3);

    //删
    queue1.pop();

    int front = queue1.front();
    int back = queue1.back();
    int size = queue1.size();
    LOG("队头元素是:%d", front);
    LOG("队尾元素是:%d", back);
    LOG("队列元素个数是:%d", size);

    //队列不建议遍历
}
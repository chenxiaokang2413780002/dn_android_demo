//
// Created by 24137 on 2019/12/22.
//
#include "Student.h"
#include "mylog.h"

Student::Student() {

    LOG("无参构造方法");

}

Student::Student(int age, string name, string sex):
    m_iAge(age),
    m_strName(name),
    m_strSex(sex){

    LOG("这是有参构造方法");

}

Student::~Student() {

    LOG("析构函数");

}

void Student::setAge(int age) {

    m_iAge = age;

}

int Student::getAge() {
    return m_iAge;
}

string Student::getSex() {
    return m_strSex;
}

string Student::getName() {
    return m_strName;
}
//
// Created by zztzt on 2019/12/24.
//

#include <iostream>
#include <string>
#include <vector>
#include "mylog.h"
#include "testVector.h"

using namespace std;

vector<string> allName;

void test01(){
    allName.push_back("111");
    allName.push_back("222");
    allName.push_back("333");

    allName.clear();
    LOG("容器的内存大小是：%d", allName.capacity());

    // 替换的方式：来解决此问题
    vector<string> tempVector; // 定义临时容器目的：就是为了解决 、 替换全局容器，让全局容器不占用内存空间
    tempVector.swap(allName); // 把全局的 全部移动 给临时 == 把全局的给回收了
    LOG("容器的内存大小是：%d", allName.capacity());
} // 函数一弹栈  tempVector给回收了，就保证了，全局和临时全部回收完毕



void main_testVector(){

    // C++  中容器，分为两种类型 vector栈的数据结构
    // 1.序列式容器：元素的排序关系，和元素本身没有任何关系，是我们在添加的时候导致的顺序导致的排序
    vector<int> vec01(1);
    vector<const char *> vec02(100, "肚子腾");

    vector<const char *> vec03;
    //添加元素
    vec03.push_back("A");
    vec03.push_back("B");
    vec03.push_back("C");
    //删除元素
    vec03.pop_back();

    //查
    const char * value1 = vec03.at(0);
    const char * value2 = vec03.at(1);

    LOG("第一个元素是：%s", value1);
    LOG("第二个元素是：%s", value2);

//    遍历
    vector<const char *>::iterator beginResult = vec03.begin();
    vector<const char *>::iterator endResult = vec03.end();
    for(;beginResult != endResult; beginResult++){
        LOG("遍历到的元素是：%s", *(beginResult));
    }

    vec03.clear();

    LOG("此容器的内存空间是：%d", vec03.capacity());

//    test01();
}
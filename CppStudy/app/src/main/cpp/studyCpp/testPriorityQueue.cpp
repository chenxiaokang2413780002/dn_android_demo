//
// Created by zztzt on 2019/12/26.
// 优先级队列
//
#pragma once

#include <iostream>
#include <queue>
#include <vector>
#include "mylog.h"
#include "testPriorityQueue.h"

using namespace std;

//自定义排序方式
class MyType
{
public:
    int count;

    // 构造函数
    MyType(int count)
    {
        this->count = count;
    }
};

// 模范源码，定义自己的排序规则
struct MyTypeLess
{
    constexpr bool operator()(const MyType& _Left, const MyType& _Right) const
    {
        return (_Left.count < _Right.count);
    }
};

void main_testPriorityQueue(){

    // 默认采用此类型：less：把最大的值，放在最前面了
//    priority_queue<int> priorityQueue;
//    priorityQueue.push(6);
//    priorityQueue.push(9);
//    priorityQueue.push(1);
//
//    LOG("最前面的值是：%d", priorityQueue.top());
//    LOG("容器元素个数：%d", priorityQueue.size());

    // 优先级队列priority_queue，基于vector实现的
    // greater：把最小的值，放在最前面了
    // less：把最大的值，放在最前面了
//    priority_queue<int, vector<int>, greater<int>> priorityQueue2;
//    priorityQueue2.push(6);
//    priorityQueue2.push(9);
//    priorityQueue2.push(1);
//
//    LOG("最前面的值是：%d", priorityQueue2.top());
//    LOG("容器元素个数：%d", priorityQueue2.size());

// 自定义排序方式
    priority_queue<MyType, vector<MyType>, MyTypeLess> priorityQueue3;
    priorityQueue3.push(MyType(1));
    priorityQueue3.push(MyType(87784));
    priorityQueue3.push(MyType(6));

    LOG("最前面的值是：%d", priorityQueue3.top());
    LOG("容器元素个数：%d", priorityQueue3.size());

}
//
// Created by zztzt on 2019/12/26.
//
#pragma once;

#include <iostream>
#include <string>
#include <set>
#include <vector>
#include "testSet.h"
#include "mylog.h"

using namespace std;

void main_testSet() {

    set<int> set1 = {1, 2, 3, 4, 5};
    //增
    set1.insert(777);
    set1.insert(888);
    set1.insert(999);

    //改
    set1.insert(1); // 重复的元素添加不进去，因为set不允许添加重复的元素

    //删除元素
    set1.erase(1);

    //查
    set<int>::iterator iterator1 = set1.find(777);
    LOG("查找到的元素是:%d", *iterator1);

    //set遍历
    set<int>::iterator beginResult = set1.begin();
    set<int>::iterator endResult = set1.end();
    for (; beginResult != endResult; beginResult++) {
        LOG("查找的元素是%d", *beginResult);
    }
}
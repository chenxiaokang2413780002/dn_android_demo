//
// Created by 24137 on 2019/12/22.
//

#ifndef CPPSTUDY_MYINSTANCE_H
#define CPPSTUDY_MYINSTANCE_H

class MyInstance {

private:
    static MyInstance *instance;
    MyInstance();
    ~MyInstance();

public:
    static MyInstance *getInstance();

    void show();
    void show2();

    void unInstance();  //单例对象需要释放，不然会造成内存泄漏

};


#endif //CPPSTUDY_MYINSTANCE_H

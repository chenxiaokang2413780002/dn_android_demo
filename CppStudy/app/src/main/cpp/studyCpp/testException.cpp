//
// Created by zztzt on 2019/12/24.
//

#include <iostream>
#include <string>
#include "mylog.h"
#include "testException.h"

using namespace std;

//异常测试
void exceptionMethod01() {
    throw "我抛出了异常";
}

//异常测试2
void exceptionMethod02() {
    throw exception();
}

//异常测试3 --自定义异常
class CustomException : public exception {
public:
    const char *what() const noexcept override {
        return "我要抛出自定义异常了";
    }
};

void exceptionMethod03() {
    throw CustomException();
}

//异常测试4 自定义异常
class Student {
public:
    char *getInfo(){
        return "自定义异常4";
    }
};

void exceptionMethod04(){
    Student s;
    throw s;
}

void main_testException() {

    try {
        exceptionMethod01();
    } catch (const char *errMsg) {
        LOG("捕获到了异常信息：%s", errMsg);
    }

    try {
        exceptionMethod02();
    } catch (exception &errMsg) {
        LOG("捕获到了异常信息：%s", errMsg.what());
    }

    try {
        exceptionMethod03();
    } catch (CustomException &errMsg) {
        LOG("捕获到了异常信息：%s", errMsg.what());
    }

    try {
        exceptionMethod04();
    } catch (Student &errMsg) {
        LOG("捕获到了异常信息：%s", errMsg.getInfo());
    }
}
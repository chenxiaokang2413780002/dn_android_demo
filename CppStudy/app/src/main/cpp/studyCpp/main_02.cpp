//
// Created by zztzt on 2019/12/24.
//
#include <iostream>
#include <string>
#include <regex>
#include "mylog.h"
#include "main_02.h"
#include "MyExtends.h"

using namespace std;  //

//特殊的操作符重载
void *operator new(size_t size) {
    LOG("有人new我了，大小是:%d", size);
    return malloc(size);
}

void operator delete(void *p) {
    LOG("有人delete我了，内存地址是:%#x", p);
}

//模板编程 普通模板 类似java泛型
template<class T>
T getMax(T number1, T number2) {
    return number1 > number2 ? number1 : number2;
}

//模板编程 类模板
template<class T1, class T2, class T3, class T4>
class TestClass {

public:
    T1 getType(T1 c) {
        return c;
    }

    T2 getType(T2 c) {
        return c;
    }

    T3 getType(T3 c) {
        return c;
    }

    T4 getType(T4 c) {
        return c;
    }
};

void main_02() {

//    LOG("max=%d", getMax(10, 5));
//    TestClass<int, string , double, char> testClass;
//    testClass.getType(5);
//    testClass.getType("5");
//    testClass.getType('5');

//    Workder workder;
//    workder.name = "chen";
//    workder.show01();
//// 静态多态，是在编译期，编译的时候我只看左边的是谁
//    Person *person = new Workder;
//    person->show01();
//    delete person;
//
//// 动态多态，是在运行期，运行的时候，去查看父类的方法是否定义了，虚函数
//    Person *person2 = new Workder;
//    person2->show04();
//    delete person2;

//  类型转换
    const char *c1 = "0x9837788";
    char *c2 = const_cast<char *>(c1);
    LOG("c2 = %s", c2);

    char *c3 = "0x98899999";
    const char *c4 = const_cast<const char *>(c3);
    LOG("c4 = %s", c4);
}


//
// Created by 24137 on 2019/12/29.
// 线程安全问题，互斥锁
//
#include <string>
#include <pthread.h>
#include <unistd.h>
#include <queue>
#include "mylog.h"
#include "testUpdateThread.h"

using namespace std;

queue<int> saveAllData;
pthread_mutex_t mutex; //互斥锁

//自定义一个函数，异步任务使用
void *customPThreadUpdateMethod(void *value) {

    //线程加锁
    pthread_mutex_lock(&mutex);

    int tempValue = *static_cast<int *>(value);
//    int tempValue = *((int *)value);

    LOG("accept value from out method is %d", tempValue);

    if (!saveAllData.empty()) {
        LOG("获取队列的数据:%d\n", saveAllData.front());
        saveAllData.pop(); // 把数据弹出去，删除的意思
    } else {
        LOG("队列中没有数据了\n");
    }

    // 解除锁定，是为了让其他多线程，可以进来的操作，这就是解锁的目的
    pthread_mutex_unlock(&mutex);

    return 0;  //必须返回一个值，不然程序会闪退
}

void main_testUpdateThread(){

    pthread_mutex_init(&mutex, nullptr);

    for (int i = 1000; i < 10010; ++i) {
        saveAllData.push(i);
    }

    pthread_t pThreadID[10];
    for (int i = 0; i < 10; ++i) {
        pthread_create(&pThreadID[i], 0, customPThreadUpdateMethod, &i);
        pthread_join(pThreadID[i], 0);
    }

    LOG("函数已经执行到了这里");

    //回收互斥锁
    pthread_mutex_destroy(&mutex);
}
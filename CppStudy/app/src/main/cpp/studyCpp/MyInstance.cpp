//
// Created by 24137 on 2019/12/22.
//
// 单例类
#include "MyInstance.h"
#include "mylog.h"

MyInstance *MyInstance::instance;

MyInstance::MyInstance() {
    LOG("MyInstance");
}

MyInstance::~MyInstance() {
    LOG("~MyInstance");
}

MyInstance *MyInstance::getInstance() {
    // C++11之后，内部会自动保证，所有static成员变量，线程的安全问题，内部已经处理好了，我们不用关心了
    if(!instance){
        instance = new MyInstance();
    }
    return instance;
}

void MyInstance::unInstance() {
    if(instance){
        delete instance;
        instance = nullptr;
    }
}

void MyInstance::show() {
    LOG("show");
}

void MyInstance::show2() {
    LOG("show2");
}
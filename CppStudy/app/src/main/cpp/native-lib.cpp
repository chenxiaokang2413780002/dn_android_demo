#include <jni.h>
#include <string>
#include <android/log.h>
#include <pthread.h>

//#include "studyCpp/main.h"
//#include "studyCpp/main_02.h"
//#include "studyCpp/testException.h"
//#include "studyCpp/testFile.h"
//#include "studyCpp/testVector.h"
//#include "studyCpp/testThread.h"
//#include "studyCpp/testUpdateThread.h"
//#include "studyCpp/testSet.h"
//#include "studyCpp/testPriorityQueue.h"
//#include "studyCpp/testMap.h"
//#include "studyCpp/testQueue.h"
//#include "studyCpp/testStack.h"

#define TAG "narkang"

#define LOG(...)__android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)

//https://blog.csdn.net/u014390957/article/details/77338504
extern "C" JNIEXPORT jstring JNICALL
Java_com_narkang_cppstudy_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";

//    main_testUpdateThread();

    __android_log_print(ANDROID_LOG_ERROR, "Tag", "这是Log测试");

    return env->NewStringUTF(hello.c_str());
}

/**
 * JNIEXPORT : Linux 和 Windows jni.h内部的宏定义 是不一样的，此宏代表是 对外暴露的标准形式，
 *             例如：在Windows中 对外暴露的标准已经被规定好了，所以在jni.h中的宏是以Windows
 *             对外暴露的标准规则来写的
 *
 * JNICALL ： Linux 和 Windows jni.h内部的宏定义 是不一样的，此宏代表是 此函数形参压栈的规则制定，
 *            例如：在Windows平台中里面的宏定义，代表函数压栈从右到左方式操作的 等等
 */
//测试 string和int，以及对应的数组
extern "C"
JNIEXPORT void JNICALL
Java_com_narkang_cppstudy_MainActivity_testStringAndInt(JNIEnv *env,  //JVM所携带的参数
                                                        jobject thiz, //谁调用了此方法，谁就是thiz
                                                        jint count,
                                                        jstring text,
                                                        jintArray count_array,
                                                        jobjectArray text_array) {

    //从java传过来的int
    int cCount = count;
    LOG("cCount=%d", cCount);

    //从java传过来的string
    const char *cText = env->GetStringUTFChars(text, NULL);
    LOG("cText=%s", cText);
    env->ReleaseStringUTFChars(text, cText);

    //从java传过来的int[]
    int *cIntArray = env->GetIntArrayElements(count_array, NULL);

    jsize size = env->GetArrayLength(count_array);
    for (int i = 0; i < size; ++i) {
        *(cIntArray + i) += 100;
        LOG("cIntArray[%d] = %d", i, *(cIntArray + i));
    }

    // 一定要对应的释放，否则会有问题
    //  void ReleaseIntArrayElements(jintArray array, jint* elems,jint mode)
    // 第三个参数：mode：模式，0:【刷新Java数组】 并 释放CC++数组
    //                     1(JNI_COMMIT): 只提交 只刷新Java数组，不释放
    //                     2(JNI_ABORT): 只释放
    env->ReleaseIntArrayElements(count_array, cIntArray, 0);

    jsize strSize = env->GetArrayLength(text_array);
    for (int i = 0; i < strSize; ++i) {
        jstring jobj = static_cast<jstring>(env->GetObjectArrayElement(text_array, i));
        const char *cTextString = env->GetStringUTFChars(jobj, NULL);
        LOG("cTextString[%d] = %s", i, cTextString);
        //用完释放
        env->ReleaseStringUTFChars(jobj, cTextString);
    }
}

//TODO 测试通过native方法更新java中的自定义对象
extern "C"
JNIEXPORT void JNICALL
Java_com_narkang_cppstudy_MainActivity_testUpdateStudent(JNIEnv *env,
                                                         jobject thiz,
                                                         jobject student) {

    //1.获取字节码
    const char *student_class_str = "com/narkang/cppstudy/Student";
    jclass student_class = env->FindClass(student_class_str);

    //2.获取方法对象
    const char *sig = "(Ljava/lang/String;)V";
    jmethodID setName = env->GetMethodID(student_class, "setName", sig);

    sig = "(I)V";
    jmethodID setAge = env->GetMethodID(student_class, "setAge", sig);

    sig = "()V";
    jmethodID myStaticMethod = env->GetStaticMethodID(student_class, "myStaticMethod", sig);

    //3.调用对象
    const char *c = "来自native的更改";
    jstring str = env->NewStringUTF(c);
    env->CallVoidMethod(student, setName, str);
    env->CallVoidMethod(student, setAge, 999);
    env->CallStaticVoidMethod(student_class, myStaticMethod);

    env->DeleteLocalRef(student_class);
    env->DeleteLocalRef(student);

}

//TODO 测试修改方法参数中的自定义对象
extern "C"
JNIEXPORT void JNICALL
Java_com_narkang_cppstudy_MainActivity_testStudentInPeopleUpdate(JNIEnv *env, jobject thiz) {

    //直接创建Person对象
    const char *person_class_str = "com/narkang/cppstudy/Person";
    jclass person_class = env->FindClass(person_class_str);
    jobject person = env->AllocObject(person_class);
    //setStudent的方法签名
    const char *sig = "(Lcom/narkang/cppstudy/Student;)V";
    jmethodID setStudent = env->GetMethodID(person_class, "setStudent", sig);

    //直接创建Student对象
    const char *student_class_str = "com/narkang/cppstudy/Student";
    jclass student_class = env->FindClass(student_class_str);
    jobject student = env->AllocObject(student_class);
    //setName方法签名
    sig = "(Ljava/lang/String;)V";
    jmethodID setName = env->GetMethodID(student_class, "setName", sig);
    //调用java带string方法的参数，需要转化为jstring传入调用方法
    jstring value = env->NewStringUTF("你是谁");
    env->CallVoidMethod(student, setName, value);
    //setAge方法签名
    sig = "(I)V";
    jmethodID setAge = env->GetMethodID(student_class, "setAge", sig);
    env->CallVoidMethod(student, setAge, 12);

    //调用Person中的setStudent方法
    env->CallVoidMethod(person, setStudent, student);

    //释放对象内存
    env->DeleteLocalRef(student);
    env->DeleteLocalRef(student_class);
    env->DeleteLocalRef(person);
    env->DeleteLocalRef(person_class);
}

//todo 测试实例化指定构造方法的对象

jclass dog_class;

extern "C"
JNIEXPORT void JNICALL
Java_com_narkang_cppstudy_MainActivity_testConstructionMethod(JNIEnv *env, jobject thiz) {

    //实例化指定的构造方法对象
    if(dog_class == NULL){
        const char *dog_class_str = "com/narkang/cppstudy/Dog";
        jclass temp = env->FindClass(dog_class_str);
        dog_class = static_cast<jclass>(env->NewGlobalRef(temp));

        LOG("对象被创建了");
    }

    const char *sig = "()V";
    const char *methodId = "<init>";
    jmethodID construction = env->GetMethodID(dog_class, methodId, sig);
    env->NewObject(dog_class, construction);
}

//todo 测试手动释放对象
extern "C"
JNIEXPORT void JNICALL
Java_com_narkang_cppstudy_MainActivity_testUnDog(JNIEnv *env, jobject thiz) {
    if(dog_class != NULL){
        LOG("对象被释放了");
        env->DeleteGlobalRef(dog_class);
        dog_class = NULL;
    }
}

//todo 动态注册部分

void testDynamicRegister01(JNIEnv *env, jobject thiz){

    LOG("动态函数Register01注册了");

}

void testDynamicRegister02(JNIEnv *env, jobject thiz){

    LOG("动态函数Register02注册了");

}

//todo 线程测试部分
jobject instance;
JavaVM *jvm;

void *customThread(void * pVoid){

    JNIEnv *env = NULL;
    //将native线程附加到jvm
    int result = jvm->AttachCurrentThread(&env, 0);
    if(result != 0){
        return 0;
    }

    jclass mainActivityClass = env->GetObjectClass(instance);
    const char *sig = "()V";
    jmethodID updateTextUI = env->GetMethodID(mainActivityClass, "updateTextUI", sig);
    env->CallVoidMethod(instance, updateTextUI);

    //解除附加到JVM的native线程
    jvm->DetachCurrentThread();

    return 0;
}

jint testActivityUiUpdate(JNIEnv *env, jobject thiz){
    instance = env->NewGlobalRef(thiz);

    pthread_t pthreadId;
    pthread_create(&pthreadId, 0, customThread, NULL);

    return 0;
}

void testUnThread(JNIEnv *env, jobject thiz){
    if(NULL != instance){
        env->DeleteGlobalRef(instance);
        instance = NULL;
    }
}

static const JNINativeMethod jniNativeMethod[] = {
        {"testDynamicRegisterFromJava01", "()V", (void *)(testDynamicRegister01)},
        {"testDynamicRegisterFromJava02", "()V", (void *)(testDynamicRegister02)},
        {"testActivityUiUpdateJava", "()I", (void *)(testActivityUiUpdate)},
        {"unThreadJava", "()V", (void *)(testUnThread)},
};

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM * javaVm, void *pVoid){

    jvm = javaVm;

    JNIEnv * jniEnv = nullptr;
    jint result = javaVm->GetEnv(reinterpret_cast<void **>(&jniEnv), JNI_VERSION_1_6);
    if(result != JNI_OK){
        return -1;
    }

    const char * mainActivityClassStr = "com/narkang/cppstudy/MainActivity";
    jclass mainActivityClass = jniEnv->FindClass(mainActivityClassStr);
    jniEnv->RegisterNatives(mainActivityClass, jniNativeMethod, sizeof(jniNativeMethod) / sizeof(JNINativeMethod));

    return JNI_VERSION_1_6;
}


extern "C"
JNIEXPORT void JNICALL
Java_com_narkang_cppstudy_MainActivity_testAutoPtr(JNIEnv *env, jobject thiz) {



}

void throwByName(JNIEnv *env, const char *name, const char *msg){
    jclass cls = env -> FindClass(name);
    if(cls != nullptr){
        env->ThrowNew(cls, msg);
    }

    env->DeleteLocalRef(cls);
}

//todo 异常处理

//Native 抛出 Java 中的异常
extern "C"
JNIEXPORT void JNICALL
Java_com_narkang_cppstudy_MainActivity_testException(JNIEnv *env, jobject thiz) {
    throwByName(env, "java/lang/IllegalArgumentException", "native throw exception");
}

//Native 调用 Java 方法时的异常
extern "C"
JNIEXPORT void JNICALL
Java_com_narkang_cppstudy_MainActivity_doit(JNIEnv *env, jobject thiz) {

    jthrowable exc;
    jclass pJclass = env->FindClass("com/narkang/cppstudy/ExceptionTest");
    jobject pJobject;

    if(pJclass == nullptr){
        return;
    }
    jmethodID initMethod = env->GetMethodID(pJclass, "<init>", "()V");
    if(initMethod == nullptr){
        return;
    }
    pJobject = env->NewObject(pJclass, initMethod);

    jmethodID mid = env->GetMethodID(pJclass, "callback", "()V");

    if(mid == nullptr){
        return;
    }

    env->CallVoidMethod(pJobject, mid);

    //检查是否发生了异常
    exc = env->ExceptionOccurred();

    if(exc){
        LOG("发生了异常");
        jclass newExcCls;
        // 打印异常日志
        env->ExceptionDescribe();
        // 这行代码才是关键不让应用崩溃的代码，
        env->ExceptionClear();

        // 发生异常了要记得释放资源
        env->DeleteLocalRef(pJobject);
        env->DeleteLocalRef(pJclass);

        newExcCls = env->FindClass("java/lang/IllegalArgumentException");
        if (newExcCls == NULL) {
            return;
        }
        env->ThrowNew(newExcCls, "Thrown from C++ code");
    } else{
        LOG("没有发生异常");
        env->DeleteLocalRef(pJobject);
        env->DeleteLocalRef(pJclass);
    }

}
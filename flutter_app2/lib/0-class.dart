

void main(){

  var point = const Point(10, 10);
  var point2 = const Point(10, 10);

  print(point == point2);
  print(identical(point2, point));

}



//num 是int和double父类，int就是long类型
class Rect{

  num left;
  num right;
  num top;
  num bottom;

  Rect(this.left, this.right, this.top, this.bottom);

  num get height => top - bottom;
  num get width => right - left;

}

//自定义注解，flutter中拿不到
@Point(10, 10)
getInfo(){

}

//常量构造函数
class Point{

    final x;
    final y;

    const Point(this.x, this.y);

}
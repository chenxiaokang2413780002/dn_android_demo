import 'package:flutter/material.dart';

import '7-sliver.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('CupertinoDemo'),
        ),
        body: DismissibleDemo(),
      ),
    );
  }
}

class DismissibleDemo extends StatefulWidget {
  @override
  _DismissibleDemoState createState() => _DismissibleDemoState();
}

class _DismissibleDemoState extends State<DismissibleDemo> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 20,
        itemBuilder: (context, index) {
          return Dismissible(
            background: Container(
              color: Colors.greenAccent,
            ),
            resizeDuration: Duration(seconds: 1),
            direction: DismissDirection.endToStart,
            movementDuration: Duration(seconds: 1),
            onDismissed: (DismissDirection direction) {
            },
            child: ListTile(
              leading: GirlWidget(),
              title: Text('$index 号技师'),
              subtitle: Text('擅长推拿，足浴'),
              trailing: Icon(Icons.arrow_forward_ios),
            ),
            key: ObjectKey(index),
          );
        });
  }
}

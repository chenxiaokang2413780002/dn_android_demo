import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.greenAccent),
      home: TextDemo(),
    );
  }
}

class TextDemo extends StatefulWidget {
  @override
  _TextDemoState createState() => _TextDemoState();
}

class _TextDemoState extends State<TextDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('TextDemo'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 300,
              child: Text(
                'TextTextDemoTextDemoTextDemoTextDemoTextDemoTextDemoTextDemoTextDemoDemo',
                textScaleFactor: 1.5,
                maxLines: 2,
                overflow: TextOverflow.fade,
                softWrap: false,
              ),
            )
          ],
        ),
      ),
    );
  }
}

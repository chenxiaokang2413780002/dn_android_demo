import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('CupertinoDemo'),
        ),
        body: DrawerDemo(),
      ),
    );
  }
}

class DrawerDemo extends StatefulWidget {
  @override
  _DrawerDemoState createState() => _DrawerDemoState();
}

class _DrawerDemoState extends State<DrawerDemo> {
  List<List<Offset>> _path = [];

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerDown: (event) {
        setState(() {
          _path.add([event.localPosition]);
        });
      },
      onPointerMove: (event) {
        setState(() {
          _path[_path.length ].add(event.localPosition);
        });
      },
      onPointerUp: (event) {
        setState(() {
          _path[_path.length ].add(event.localPosition);
        });
      },
      onPointerCancel: (event) {
        setState(() {
          _path[_path.length ].add(event.localPosition);
        });
      },
      child: CustomPaint(
        painter: DrawerPen(_path),
        child: Container(
          width: double.infinity,
          height: double.infinity,
        ),
      ),
    );
  }
}

class DrawerPen extends CustomPainter {
  List<List<Offset>> _list;

  DrawerPen(this._list);

  Paint _paint = Paint()
    ..color = Colors.lightBlueAccent
    ..style = PaintingStyle.stroke
    ..strokeWidth = 3.0;

  @override
  void paint(Canvas canvas, Size size) {
    _list.forEach((list) {
      Path path = Path();
      for (int i = 0; i < list.length; i++) {
        if (i == 0) {
          path.moveTo(list[i].dx, list[i].dy);
        }else{
          path.lineTo(list[i].dx, list[i].dy);
        }
      }
      canvas.drawPath(path, _paint);
    });
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return true;
  }
}

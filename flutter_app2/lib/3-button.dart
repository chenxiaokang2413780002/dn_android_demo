import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.greenAccent),
      home: ButtonDemo(),
    );
  }
}

class ButtonDemo extends StatefulWidget {
  @override
  _TextDemoState createState() => _TextDemoState();
}

class _TextDemoState extends State<ButtonDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('TextDemo'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text('RaisedButton'),
              onPressed: () {},
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                side: BorderSide(
                  width: 2.0,
                  color: Colors.redAccent
                )
              ),
            ),
            FlatButton(
              child: Text('FlatButton'),
              onPressed: (){},
            ),
            OutlineButton(
              child: Text('OutlineButton'),
              onPressed: (){},
            )
          ],
        ),
      ),
    );
  }
}


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.greenAccent
      ),
      home:Scaffold(
        appBar: AppBar(
          title: Text('SheetDemo'),
        ),
        body: SheetDemo(),
      )
    );
  }
}

class SheetDemo extends StatefulWidget {
  @override
  _SheetDemoState createState() => _SheetDemoState();
}

class _SheetDemoState extends State<SheetDemo> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: RaisedButton(
        child: Text('SheetButton'),
        onPressed: (){
          showBottomSheet(context: context, builder: (context){
            return CupertinoActionSheet(
              title: Text('这是标题'),
              message: Text('这是协议相关内容'),
              actions: <Widget>[
                CupertinoActionSheetAction(
                  child: Text('yes'),
                  onPressed: (){},
                ),
                CupertinoActionSheetAction(
                  child: Text('no'),
                  onPressed: (){},
                ),
              ],
              cancelButton: CupertinoButton(
                child: Text('取消'),
                onPressed: (){},
              ),
            );
          });
        },
      ),
    );
  }
}


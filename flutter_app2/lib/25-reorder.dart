import 'package:flutter/material.dart';

import '7-sliver.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('CupertinoDemo'),
        ),
        body: ReorderableListViewDemo(),
      ),
    );
  }
}

class ReorderableListViewDemo extends StatefulWidget {
  @override
  _ReorderableListViewDemoState createState() =>
      _ReorderableListViewDemoState();
}

class _ReorderableListViewDemoState extends State<ReorderableListViewDemo> {
  List _list = List.generate(20, (index) => index);

  @override
  Widget build(BuildContext context) {
    return ReorderableListView(
      children: _list.map((value) {
        return ListTile(
          key: ObjectKey(value),
          leading: GirlWidget(),
          title: Text('$value 号技师'),
          subtitle: Text('$value，足浴'),
          trailing: Icon(Icons.arrow_forward_ios),
        );
      }).toList(),

      onReorder: (oldIndex,newIndex){
        setState(() {
          if(oldIndex<newIndex){
            newIndex-=1;
          }
          var item = _list.removeAt(oldIndex);
          _list.insert(newIndex, item);
        });
      },
    );
  }
}

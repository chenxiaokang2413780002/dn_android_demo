import 'dart:math';

import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('CupertinoDemo'),
        ),
        body: WrapDemo(),
      ),
    );
  }
}

class WrapDemo extends StatefulWidget {
  @override
  _WrapDemoState createState() => _WrapDemoState();
}

class _WrapDemoState extends State<WrapDemo> {
  List _list = List.generate(50, (index) => index);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Wrap(
        spacing: 10.0,
        runSpacing: 20.0,
        children: _list.map((value) {
          double widen = Random().nextDouble()*100+50;
          return Container(
            width: widen,
            height: 80,
            color: Colors.primaries[Random().nextInt(12)],
          );
        }).toList(),
      ),
    );
  }
}

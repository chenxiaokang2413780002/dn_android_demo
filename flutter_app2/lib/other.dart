import 'package:flutter/material.dart';
import '8-route.dart';

class SecondPage extends StatefulWidget {

  final msg;

  SecondPage({this.msg});

  @override
  _SecondPageState createState() => _SecondPageState(msg: msg);
}

class _SecondPageState extends State<SecondPage> {

  final msg;

  _SecondPageState({this.msg}){
    print('_SecondPageState B');
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print('initState B');
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    print('didChangeDependencies B');
  }

  @override
  Widget build(BuildContext context) {
    print('build B');
    Message arguments = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('接受消息 msg ${msg?.msg} name ${msg?.name}'),
            RaisedButton(
              child: Text('返回'),
              onPressed: (){
                Navigator.pop(context, "这是来自第二个页面的消息");
              },
            )
          ],
        ),
      ),
    );
  }

  @override
  void didUpdateWidget(SecondPage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    print('didUpdateWidget B');
  }

  @override
  void deactivate() {
    super.deactivate();
    print('deactivate B');
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print('dispose B');
  }
}


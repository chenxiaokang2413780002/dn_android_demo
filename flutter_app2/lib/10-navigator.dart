import 'package:flutter/material.dart';
import 'package:flutterapp2/other.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green,
      ),
      home: BottomViewDemo(),
    );
  }
}

class BottomViewDemo extends StatefulWidget {
  @override
  _BottomViewDemoState createState() => _BottomViewDemoState();
}

class _BottomViewDemoState extends State<BottomViewDemo> {
  var _current = 0;

  List _list = [NavigatorDemo(), PageB(), PageC(), PageD()];

  _choose(index) {
    setState(() {
      _current = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BottomViewDemo'),
      ),
      body: _list[_current],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _current,
        onTap: (index) => _choose(index),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('首页'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.message),
            title: Text('消息'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people),
            title: Text('好友'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.email),
            title: Text('邮箱'),
          )
        ],
      ),
    );
  }
}

class PageA extends StatefulWidget {
  @override
  _PageAState createState() => _PageAState();
}

class _PageAState extends State<PageA> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text('首页'),
          RaisedButton(
            child: Text('局部跳转'),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                  builder:(context) => SecondPage(),
              ));
            },
          ),
        ],
      ),
    );
  }
}

class NavigatorDemo extends StatefulWidget {
  @override
  _NavigatorDemoState createState() => _NavigatorDemoState();
}

class _NavigatorDemoState extends State<NavigatorDemo> {
  @override
  Widget build(BuildContext context) {
    return Navigator(
      onGenerateRoute: (settings){
        return MaterialPageRoute(builder: (context) => PageA());
      },
    );
  }
}


class PageB extends StatefulWidget {
  @override
  _PageBState createState() => _PageBState();
}

class _PageBState extends State<PageB> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('消息'),
    );
  }
}

class PageC extends StatefulWidget {
  @override
  _PageCState createState() => _PageCState();
}

class _PageCState extends State<PageC> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('好友'),
    );
  }
}

class PageD extends StatefulWidget {
  @override
  _PageDState createState() => _PageDState();
}

class _PageDState extends State<PageD> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('邮箱'),
    );
  }
}

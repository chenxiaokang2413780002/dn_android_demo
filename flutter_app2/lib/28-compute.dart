import 'package:flutter/material.dart';

main() => runApp(MyApp());

//https://www.cnblogs.com/lxlx1798/p/11172246.html
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.green),
      home: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            title: Text('CalculatorDemo'),
          ),
          body: Center(child: CalculatorDemo())),
    );
  }
}

class CalculatorDemo extends StatefulWidget {
  @override
  _CalculatorDemoState createState() => _CalculatorDemoState();
}

class _CalculatorDemoState extends State<CalculatorDemo> {
  var text = '';
  var operate = '';
  var beforeText = '';
  bool _isOperate = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Container(
            alignment: Alignment.bottomRight,
            padding: EdgeInsets.only(right: 20),
            child: Text(
              '$text',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 40,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
        CalculatorBody(_Compute),
      ],
    );
  }

  double _valueToDouble(String value) {
    return double.parse(value);
  }

  //计算值 回调函数
  _Compute(value) {
    setState(() {
      switch (value) {
        case 'AC':
          text = '0';
          _isOperate = true;
          operate = '';
          beforeText = '';
          break;
        case '+/-':
          if(text.startsWith('-')){
            text=text.substring(1);
          }else{
            text='-$text';
          }
          break;
        case '%':
          text = '${_valueToDouble(text) / 100}';
          break;
        case '+':
        case '÷':
        case 'X':
        case '-':
          beforeText = text;
          operate = value;
          _isOperate = true;
          break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case '.':
          if (_isOperate) {
            text = '';
            _isOperate = false;
          }
          text += value;
          if (text.startsWith('.')) {
            text = '0.';
          }
          if (text.startsWith('00')) {
            text = '0';
          }
          break;
        case '=':
          double d1 = _valueToDouble(beforeText);
          double d2 = _valueToDouble(text);
          print('d1= $d1  d2= $d2');
          switch (operate) {
            case '+':
              text = '${d1 + d2}';
              break;
            case '-':
              text = '${d1 - d2}';
              break;
            case 'X':
              text = '${d1 * d2}';
              break;
            case '÷':
              text = '${d1 / d2}';
              break;
          }
          beforeText = '';
          _isOperate = true;
          break;
      }
    });
  }
}

class CalculatorBody extends StatelessWidget {
  ValueChanged<String> valueChanged;

  CalculatorBody(this.valueChanged);

  List<Map> _listData = [
    {
      'textColor': Colors.black,
      'color': Color(0xffa5a5a5),
      'highlightColor': Color(0xffd8d8d8),
      'text': 'AC'
    },
    {
      'textColor': Colors.black,
      'color': Color(0xffa5a5a5),
      'highlightColor': Color(0xffd8d8d8),
      'text': '+/-'
    },
    {
      'textColor': Colors.black,
      'color': Color(0xffa5a5a5),
      'highlightColor': Color(0xffd8d8d8),
      'text': '%'
    },
    {
      'color': Color(0xffe89e28),
      'highlightColor': Color(0xffedc68f),
      'text': '÷'
    },
    {
      'color': Color(0xff363636),
      'highlightColor': Color(0xff5a5a5a),
      'text': '7'
    },
    {
      'color': Color(0xff363636),
      'highlightColor': Color(0xff5a5a5a),
      'text': '8'
    },
    {
      'color': Color(0xff363636),
      'highlightColor': Color(0xff5a5a5a),
      'text': '9'
    },
    {
      'color': Color(0xffe89e28),
      'highlightColor': Color(0xffedc68f),
      'text': 'X'
    },
    {
      'color': Color(0xff363636),
      'highlightColor': Color(0xff5a5a5a),
      'text': '4'
    },
    {
      'color': Color(0xff363636),
      'highlightColor': Color(0xff5a5a5a),
      'text': '5'
    },
    {
      'color': Color(0xff363636),
      'highlightColor': Color(0xff5a5a5a),
      'text': '6'
    },
    {
      'color': Color(0xffe89e28),
      'highlightColor': Color(0xffedc68f),
      'text': '-'
    },
    {
      'color': Color(0xff363636),
      'highlightColor': Color(0xff5a5a5a),
      'text': '1'
    },
    {
      'color': Color(0xff363636),
      'highlightColor': Color(0xff5a5a5a),
      'text': '2'
    },
    {
      'color': Color(0xff363636),
      'highlightColor': Color(0xff5a5a5a),
      'text': '3'
    },
    {
      'color': Color(0xffe89e28),
      'highlightColor': Color(0xffedc68f),
      'text': '+'
    },
    {
      'color': Color(0xff363636),
      'highlightColor': Color(0xff5a5a5a),
      'text': '0',
      'width': 165.0
    },
    {
      'color': Color(0xff363636),
      'highlightColor': Color(0xff5a5a5a),
      'text': '.'
    },
    {
      'color': Color(0xffe89e28),
      'highlightColor': Color(0xffedc68f),
      'text': '='
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 25,
      runSpacing: 15,
      children: List.generate(_listData.length, (index) {
        return CalculatorItem(
          text: _listData[index]['text'],
          color: _listData[index]['color'],
          width: _listData[index]['width'],
          highlightColor: _listData[index]['highlightColor'],
          textColor: _listData[index]['textColor'],
          valueChanged: valueChanged,
        );
      }),
    );
  }
}

class CalculatorItem extends StatelessWidget {
  final textColor;
  final width;
  final color;
  final text;
  final highlightColor;
  ValueChanged<String> valueChanged;

  CalculatorItem({
    this.valueChanged,
    this.highlightColor,
    this.width,
    this.text,
    this.textColor,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: BoxDecoration(
          color: color, borderRadius: BorderRadius.all(Radius.circular(200))),
      child: InkWell(
        onTap: () {
          valueChanged('$text');
        },
        borderRadius: BorderRadius.all(Radius.circular(200)),
        highlightColor: highlightColor,
        child: Container(
          width: width == null ? 70 : width,
          alignment: Alignment.center,
          height: 70,
          child: Text(
            '$text',
            style: TextStyle(
                color: textColor == null ? Colors.white : textColor,
                fontSize: 20),
          ),
        ),
      ),
    );
  }
}
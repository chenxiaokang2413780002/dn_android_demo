
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('CupertinoDemo'),
        ),
        body: StackDemo(),
      ),
    );
  }
}

class StackDemo extends StatefulWidget {
  @override
  _StackDemoState createState() => _StackDemoState();
}

class _StackDemoState extends State<StackDemo> {
  var _index=0;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          IndexedStack(
            index: _index,
            children: <Widget>[
              Container(
                width: 300,
                height: 150,
                color: Colors.lightBlueAccent,
                child: Icon(Icons.dialpad),
              ),
              Container(
                width: 300,
                height: 150,
                color: Colors.redAccent,
                child: Icon(Icons.event),
              ),
              Container(
                width: 300,
                height: 150,
                color: Colors.yellowAccent,
                child: Icon(Icons.print),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              IconButton(icon: Icon(Icons.dialpad),onPressed: (){
                setState(() {
                  _index=0;
                });
              },),
              IconButton(icon: Icon(Icons.event),onPressed: (){
                setState(() {
                  _index=1;
                });
              },),
              IconButton(icon: Icon(Icons.print),onPressed: (){
                setState(() {
                  _index=2;
                });
              },),
            ],
          )
        ],
      ),
    );
  }
}

class StackWidget extends StatelessWidget {
  const StackWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      overflow: Overflow.visible,
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          width: 300,
          height: 150,
          color: Colors.lightBlueAccent,
        ),
        Container(
          width: 200,
          height: 100,
          color: Colors.redAccent,
        ),
        Positioned(
          left: 275,
          top: -25,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.yellowAccent,

                shape: BoxShape.circle
            ),
            width: 50,
            height: 50,
          ),
        ),

      ],
    );
  }
}

import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.greenAccent),
      home: ImageDemo(),
    );
  }
}

class ImageDemo extends StatefulWidget {
  @override
  _ImageDemoState createState() => _ImageDemoState();
}

class _ImageDemoState extends State<ImageDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ImageDemo'),
      ),
      body: Center(
          child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'images/flutter_test.jpg',
              width: 300,
              height: 400,
            ),
            Image.network(
              'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1592745072127&di=5e2f484b269945fc2df67de55e443496&imgtype=0&src=http%3A%2F%2Fimg1.imgtn.bdimg.com%2Fit%2Fu%3D3175508956%2C2902264390%26fm%3D214%26gp%3D0.jpg',
              width: 400,
              height: 300,
            ),
            MemoryImage(),
            OpenCamera(),
            SDCardImage(),
          ],
        ),
      )),
    );
  }
}

class SDCardImage extends StatefulWidget {
  @override
  _SDCardImageState createState() => _SDCardImageState();
}

class _SDCardImageState extends State<SDCardImage> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}


class OpenCamera extends StatefulWidget {
  @override
  _OpenCameraState createState() => _OpenCameraState();
}

class _OpenCameraState extends State<OpenCamera> {

  var _img;

  _getImage() async{
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;

    Directory appDocDir = await getApplicationDocumentsDirectory();
    String appDocPath = appDocDir.path;

    var path = (await getTemporaryDirectory()).path;

  }

  _openCamera() async{
    var image = await ImagePicker().getImage(source: ImageSource.camera);
    setState(() {
      _img = File(image.path);
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _img==null?Text("请选择图片"):Image.file(_img),
        RaisedButton(
          child: Text('打开相机'),
          onPressed: _openCamera,
          color: Colors.blueAccent,
          splashColor: Colors.greenAccent,
        )
      ],
    );
  }
}

class MemoryImage extends StatefulWidget {
  @override
  _MemoryImageState createState() => _MemoryImageState();
}

class _MemoryImageState extends State<MemoryImage> {
  Uint8List _img;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getImg();
  }

  @override
  Widget build(BuildContext context) {
    return _img == null
        ? Text('没有相应资源')
        : Image.memory(
            _img,
            width: 300,
            height: 400,
          );
  }

  _getImg() async {
    await rootBundle.load("images/flutter_test.jpg").then((value) {
      if (mounted) {
        setState(() {
          _img = value.buffer.asUint8List();
        });
      }
    });
  }
}

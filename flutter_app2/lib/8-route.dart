import 'package:flutter/material.dart';
import 'package:flutterapp2/other.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primaryColor: Colors.green,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text('首页'),
          ),
          body: RoutesDemo(),
        ));
  }
}

class RoutesDemo extends StatefulWidget {
  @override
  _RoutesDemoState createState() => _RoutesDemoState();
}

class _RoutesDemoState extends State<RoutesDemo> {
  var _result;

  /// 这个函数严格意义上来讲不属于生命周期的一部分，因为这个时候State的widget属性为空，无
  ///法在构造函数中访问widget的属性 。但是构造函数必然是要第一个调用的。可以在这一部分接
  /// 收前一个页面传递过来的数据。
  /// 调用次数：1次
  _RoutesDemoState(){
    print('_RoutesDemoState A');
  }

  /// 当插入渲染树的时候调用，这个函数在生命周期中只调用一次。这里可以做一些初始化工作，比
  /// 如初始化State的变量。
  /// 调用次数：1次
  @override
  void initState() {
    super.initState();
    print('initState A');
  }

  ///这个函数会紧跟在initState之后调用，并且可以调用BuildContext.inheritFromWidgetOfExactType，
  ///那么BuildContext.inheritFromWidgetOfExactType的使用场景是什么呢？最经典的应用场景是
  ///new DefaultTabController(length: 3, child: new TabBar(
  ///      tabs: [ "主页","订单","我的" ]
  ///      .map( (data)=>new Text(data) ).toList(),
  ///
  /// 调用时机：
  ///         1.初始化时，在initState()之后立刻调用
  ///         2.当依赖的InheritedWidget rebuild,会触发此接口被调用
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print('didChangeDependencies A');
  }

  _activityResultCode(context) async {
    var result = await Navigator.push(
        context,
//        构造函数传参
        MaterialPageRoute(
            builder: (context) => SecondPage(
                  msg: Message('ruby', '来自首页的消息'),
                )));
    setState(() {
      _result = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    print('build A');
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('$_result'),
          RaisedButton(
            child: Text('点击跳转'),
            onPressed: () {
              _activityResultCode(context);
            },
          )
        ],
      ),
    );
  }

  ///  祖先节点rebuild widget时调用 .当组件的状态改变的时候就会调用didUpdateWidget.
  @override
  void didUpdateWidget(RoutesDemo oldWidget) {
    super.didUpdateWidget(oldWidget);
    print('didUpdateWidget A');
  }

  ///  组件移除  deactivate > dispose
  ///在dispose之前，会调用这个函数。实测在组件可见状态变化的时候会调用，当组件卸载时
  ///也会先一步dispose调用。
  @override
  void deactivate() {
    // TODO: implement deactivate
    super.deactivate();
    print('deactivate A');
  }

  /// 一旦到这个阶段，组件就要被销毁了，这个函数一般会移除监听，清理环境。
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print('dispose A');
  }


}

class Message {
  var name;
  var msg;

  Message(this.name, this.msg);
}

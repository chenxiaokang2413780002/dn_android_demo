
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('CupertinoDemo'),
        ),
        body: DialogDemo(),
      ),
    );
  }
}

class DialogDemo extends StatefulWidget {
  @override
  _DialogDemoState createState() => _DialogDemoState();
}

class _DialogDemoState extends State<DialogDemo> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          AlertDialog(
            title: Text('信息'),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))
            ),
            backgroundColor: Colors.lightBlueAccent,
            content: Column(
              children: <Widget>[
                Text('姓名:simon'),
                Text('学院:动脑学院'),
              ],
            ),
            actions: <Widget>[
              FlatButton(child: Text('确定',style: TextStyle(color: Colors.white),),onPressed: (){
              },),
              FlatButton(child: Text('取消',style: TextStyle(color: Colors.white),),onPressed: (){},),
            ],
          ),
          CupertinoAlertDialog(
            title: Text('信息'),
            content: Column(
              children: <Widget>[
                Text('姓名:simon'),
                Text('学院:动脑学院'),
              ],
            ),
            actions: <Widget>[
              CupertinoDialogAction(child: Text('确定'),onPressed: (){},),
              CupertinoDialogAction(child: Text('取消'),onPressed: (){},),
            ],
          ),
          SimpleDialog(
            title: Text('信息'),
            children: <Widget>[
              Text('姓名:simon'),
              Text('学院:动脑学院'),
              FlatButton(child: Text('确定',style: TextStyle(color: Colors.blueAccent),),onPressed: (){
              },),
              FlatButton(child: Text('取消',style: TextStyle(color: Colors.blueAccent),),onPressed: (){},),

            ],
          ),
          Dialog(
            shape: CircleBorder(),
            child: Column(
              children: <Widget>[
                Text('信息'),
                Text('姓名:simon'),
                Text('学院:动脑学院'),
                FlatButton(child: Text('确定',style: TextStyle(color: Colors.blueAccent),),onPressed: (){
                },),
                FlatButton(child: Text('取消',style: TextStyle(color: Colors.blueAccent),),onPressed: (){},),
              ],
            ),
          )
        ],
      ),
    );
  }
}

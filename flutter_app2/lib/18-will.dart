
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('CupertinoDemo'),
        ),
        body: WillDemo(),
      ),
    );
  }
}

class DoubleCheck extends StatefulWidget {
  @override
  _DoubleCheckState createState() => _DoubleCheckState();
}

class _DoubleCheckState extends State<DoubleCheck> {

  var outTime;

  @override
  Widget build(BuildContext context) {
    //todo
    return WillPopScope(
      onWillPop: () async{
        if(outTime==null || DateTime.now().difference(outTime).inSeconds>1){

          outTime = DateTime.now();
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('再次点击退出程序'),
          ));
          return false;
        }else{
          return true;
        }
      },
      child: Center(
        child: Text('点击两次退出'),
      ),
    );
  }
}

class WillDemo extends StatefulWidget {
  @override
  _WillDemoState createState() => _WillDemoState();
}

class _WillDemoState extends State<WillDemo> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        return showCupertinoDialog(context: context,builder: (context){
          return CupertinoAlertDialog(
            title: Text('提示'),
            content: Text('是否要退出程序'),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text('确定'),
                onPressed: (){
                  Navigator.of(context).pop(true);
                },
              ),
              CupertinoDialogAction(
                child: Text('取消'),
                onPressed: (){
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        });
      },
      child: Center(
        child: Text('返回已监听'),
      ),
    );
  }
}

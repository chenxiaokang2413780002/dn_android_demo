import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.green,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('CupertinoDemo'),
        ),
        body: GestureDetectorDemo(),
      ),
    );
  }
}

class GestureDetectorDemo extends StatefulWidget {
  @override
  _GestureDetectorDemoState createState() => _GestureDetectorDemoState();
}

class _GestureDetectorDemoState extends State<GestureDetectorDemo> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print('onTap');
      },
      onTapDown: (event) {
        print('${event.localPosition}');
      },
      onTapUp: (event) {
        print('${event.localPosition}');
      },
      onTapCancel: () {
        print('onTapCancel');
      },
      child: Container(
        width: 300,
        height: 300,
        color: Colors.redAccent,
      ),
    );
  }
}

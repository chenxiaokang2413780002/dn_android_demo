
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image/image.dart' as image;

main()=>runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.green),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Particle'),
        ),
        body: ParticleDemo(),
      ),
    );
  }
}

class ParticleDemo extends StatefulWidget {
  @override
  _ParticleDemoState createState() => _ParticleDemoState();
}

class _ParticleDemoState extends State<ParticleDemo> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ParticleBody(
            child: Container(
              width: 300,
              height: 300,
              child: Image.network('https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1593947641115&di=8f2e9b7bfe3af7e801bf9392b580c83b&imgtype=0&src=http%3A%2F%2Fe.hiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2Ff31fbe096b63f624a76a76948644ebf81a4ca376.jpg'),
            ),
          )
        ],
      ),
    );
  }
}

class ParticleBody extends StatefulWidget {

  Widget child;

  var duration;

  //图层数量
  var numberLayers;

  ParticleBody({this.duration=const Duration(seconds: 1), this.child, this.numberLayers = 10});

  @override
  _ParticleBodyState createState() => _ParticleBodyState();
}

class _ParticleBodyState extends State<ParticleBody> with TickerProviderStateMixin{

  GlobalKey _globalKey = GlobalKey();

//承载widget的数组
  List<Widget> layers=[];

  AnimationController _controller;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(vsync: this, duration: widget.duration)..addListener(() {
      setState(() {

      });
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        ...layers,
        GestureDetector(
          onTap: (){
            _grain();
          },
          child: Opacity(
            opacity: (1-_controller.value),
            //RepaintBoundary 截图组件
            child: RepaintBoundary(
              key: _globalKey,
              child: widget.child,
            ),
          ),
        )
      ],
    );
  }

  _grain() async {
    var imgInfo = await _getImgInfo();
    var width = imgInfo.width;
    var height = imgInfo.height;
    //获取相应numberLayers宽高的image.Image对象
    List<image.Image> blankImage = List.generate(widget.numberLayers, (index) => image.Image(width, height));
    //分配像素点
    _separate(blankImage, width, height, imgInfo);

    layers = blankImage.map((e) => _getLayer(e)).toList();

    setState(() {

    });

    _controller.forward(from: 0);
  }

  //把图片画到图层上
  Widget _getLayer(image.Image imgInfo){
    Uint8List uint8list = Uint8List.fromList(image.encodePng(imgInfo));

    CurvedAnimation curvedAnimation = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    Animation<Offset> animation = Tween(
        begin: Offset.zero,
        end: Offset(30, -50) +
            Offset(30, 30).scale((Random().nextDouble() - 0.5) * 2,
                (Random().nextDouble() - 0.5) * 2))
        .animate(_controller);

    return AnimatedBuilder(
      child: Image.memory(uint8list),
      builder: (context, child) {
        return Transform.translate(
          offset: animation.value,
          child: Opacity(
            //curvedAnimation.value 0.0 to 1.0
            opacity: cos(curvedAnimation.value * pi / 2), //1->0
            child: child,
          ),
        );
      },
      animation: _controller,
    );
  }

  //分配像素点
  _separate(List<image.Image> blankImage, width, height, image.Image imgInfo){
    for(int x = 0; x < width; x++){
      for(int y = 0; y < height; y++){
        var pixel = imgInfo.getPixel(x, y);
        if(pixel == 0) continue;
        int index = Random().nextInt(widget.numberLayers);
        //把像素点分配到随机不同图层上
        blankImage[index].setPixel(x, y, pixel);
      }
    }
  }

  //获取图片信息
  Future<image.Image> _getImgInfo() async{
    //通过_globalKey获取当前渲染树对象
    RenderRepaintBoundary repaintBoundary = _globalKey.currentContext.findRenderObject();
    //获取image对象
    var img = await repaintBoundary.toImage();
    //生成相应得byte数组
    var byteData = await img.toByteData(format: ImageByteFormat.png);
    //转化成Uint8List，才能编解码
    var uint8List = byteData.buffer.asUint8List();
    //解码成image.Image，可以获取图片信息
    return image.decodeImage(uint8List);
  }
}

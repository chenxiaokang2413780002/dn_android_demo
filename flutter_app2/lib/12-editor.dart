import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.red),
      home: EditorViewDemo(),
    );
  }
}

class EditorViewDemo extends StatefulWidget {
  @override
  _EditorViewDemoState createState() => _EditorViewDemoState();
}

class _EditorViewDemoState extends State<EditorViewDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('输入框'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              width: 300,
              height: 40,
              child: TextField(
                controller: TextEditingController(),
                decoration: InputDecoration(
                    hintText: 'QQ邮箱',
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        borderSide: BorderSide(
                          color: Colors.transparent,
                          width: 1.0,
                        )),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      borderSide: BorderSide(
                        color: Colors.transparent,
                        width: 1.0,
                      ),
                    )),
              ),
            )
          ],
        ),
      ),
    );
  }
}

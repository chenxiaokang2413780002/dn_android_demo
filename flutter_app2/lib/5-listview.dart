import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.greenAccent),
      home: ListViewDemo(),
    );
  }
}

class ListViewDemo extends StatefulWidget {
  @override
  _ListViewDemoState createState() => _ListViewDemoState();
}

class _ListViewDemoState extends State<ListViewDemo> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _reFreshData();
  }

  List _list;

  Future _reFreshData() async {
    await Future.delayed(Duration(seconds: 2), () {
      setState(() {
        _list = List.generate(Random().nextInt(15) + 5, (_) {});
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListViewDemo'),
      ),
      body: RefreshIndicator(
        onRefresh: _reFreshData,
        child: _list == null
            ? Center(
                child: CupertinoActivityIndicator(
                radius: 20,
              ))
            : ListView.separated(
                separatorBuilder: (BuildContext context, int index) {
                  return Divider(
                    thickness: 2,
                    color: Colors.redAccent,
                  );
                },
                itemCount: _list.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    leading: Icon(Icons.ac_unit),
                    title: Text('title${index}'),
                    subtitle: Text('subtitle$index'),
                    trailing: Icon(Icons.arrow_forward),
                  );
                }),
      ),
    );
  }

  ListView LowListView() {
    return ListView(
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.ac_unit),
          title: Text('title1'),
          subtitle: Text('subtitle1'),
          trailing: Icon(Icons.arrow_forward),
        ),
        ListTile(
          leading: Icon(Icons.ac_unit),
          title: Text('title1'),
          subtitle: Text('subtitle1'),
          trailing: Icon(Icons.arrow_forward),
        ),
        ListTile(
          leading: Icon(Icons.ac_unit),
          title: Text('title1'),
          subtitle: Text('subtitle1'),
          trailing: Icon(Icons.arrow_forward),
        ),
        ListTile(
          leading: Icon(Icons.ac_unit),
          title: Text('title1'),
          subtitle: Text('subtitle1'),
          trailing: Icon(Icons.arrow_forward),
        )
      ],
    );
  }
}

package com.example.threadlocaldemo;

import org.junit.Test;

public class DisappearRequest {

    int i = 0;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            for (int j = 0; j < 100000; j++) {
                i++;
            }
        }
    };

    @Test
    public void test01() throws InterruptedException {

        Thread thread = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        thread.start();
        thread2.start();
        thread.join();
        thread2.join();

        System.out.println(i);

    }
}

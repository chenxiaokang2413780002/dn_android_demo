package com.example.threadlocaldemo;

import androidx.annotation.Nullable;

import org.junit.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

class ThreadSafeFormatter{

//    public static ThreadLocal<SimpleDateFormat> dateFormatThreadLocal = ThreadLocal.withInitial(new Supplier<SimpleDateFormat>() {
//        @Override
//        public SimpleDateFormat get() {
//            return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        }
//    });

    public static ThreadLocal<SimpleDateFormat> dateFormatThreadLocal = new ThreadLocal<SimpleDateFormat>(){
        @Nullable
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        }
    };

}

public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    public ExecutorService threadPool = Executors.newFixedThreadPool(10);

    public String date(int seconds){
        Date date = new Date(1000*seconds);
        SimpleDateFormat dateFormat = ThreadSafeFormatter.dateFormatThreadLocal.get();
        return dateFormat.format(date);
    }

    @Test
    public void simpleDateFormatTest() throws IOException {

        for(int i = 0; i<1000; i++){
            final int finalI = i;
            threadPool.submit(new Runnable() {
                @Override
                public void run() {
                    String date = date(finalI);
                    System.out.println(date);
                }
            });
        }
        threadPool.shutdown();

        System.in.read();

    }


    @Test
    public void Test2(){
        new Service1().process();
    }

    @Test
    public void Test3(){

        ThreadLocal<Long> longThreadLocal = new ThreadLocal<>();

//        longThreadLocal.set(Thread.currentThread().getId());

        Long value = longThreadLocal.get();

        System.out.println(value);

    }

}

class UserContextHolder{
    public static ThreadLocal<User> holder = new ThreadLocal<>();
}

class User{

    String name;

    public User(String name) {
        this.name = name;
    }
}

class Service1{

    public void process(){
        User user = new User("User1");
        UserContextHolder.holder.set(user);
        new Service2().process();
    }

}

class Service2{

    public void process(){
        User user = UserContextHolder.holder.get();
        System.out.println("service2拿到用户名:"+user.name);
        user.name = "user2";
        UserContextHolder.holder.set(user);
        new Service3().process();
    }

}

class Service3{

    public void process(){
        User user = UserContextHolder.holder.get();
        System.out.println("service3拿到用户名:"+ user.name);
    }

}
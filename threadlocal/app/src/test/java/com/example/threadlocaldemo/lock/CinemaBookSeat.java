package com.example.threadlocaldemo.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CinemaBookSeat {

    private Runnable runnable01 = new Runnable() {
        @Override
        public void run() {
            bookSeat();
        }
    };
    private static Lock lock01 = new ReentrantLock();
    private static void bookSeat(){
        lock01.lock();
        try{
            System.out.println(Thread.currentThread().getName() + "开始预定座位");
            Thread.sleep(1000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }finally {
            lock01.unlock();
        }
    }

    public void test01(){
        new Thread(runnable01).start();
    }

}

package com.example.threadlocaldemo;

import org.junit.Test;

public class SynchronizedRecursion {

    private int a = 0;

    private synchronized void method(){
        System.out.println("这是method1,a = " + a);
        if(a==0){
            a ++;
            method();
        }
    }

    @Test
    public void test01(){
        method();
    }

}

package com.example.threadlocaldemo.lock;

import org.junit.Test;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockDemo {

    private Runnable01 runnable01 = new Runnable01(1);
    private Runnable01 runnable02 = new Runnable01(0);

    @Test
    public void test01() {
        Thread thread01 = new Thread(runnable01);
        Thread thread02 = new Thread(runnable02);
        thread01.start();
        thread02.start();
        while (thread01.isAlive() || thread02.isAlive()) {

        }
    }


    private static Lock lock02 = new ReentrantLock();

    //lock不会像synchronized一样在异常时自动释放锁
    //因此最佳实践是，在finally中释放锁，以保证发生异常时锁一定被释放
    @Test
    public void test02(){
        lock02.lock();
        try {
            System.out.println(Thread.currentThread().getName() + "开始执行任务");
        }finally {
            lock02.unlock();
        }
    }

    private static Lock lock03 = new ReentrantLock();
    private Runnable runnable03 = new Runnable() {
        @Override
        public void run() {
            try {
                lock03.lockInterruptibly();
                try{
                    System.out.println(Thread.currentThread().getName() + "获取到了锁");
                    Thread.sleep(5000);
                }catch (InterruptedException e){
                    System.out.println(Thread.currentThread().getName() + "睡眠期间被中断");
                }finally {
                    lock03.unlock();
                    System.out.println(Thread.currentThread().getName() + "释放了锁");
                }
            } catch (InterruptedException e) {
                System.out.println(Thread.currentThread().getName() + "等锁期间被中断");
            }

        }
    };

    @Test
    public void test03(){

        Thread thread = new Thread(runnable03);
        Thread thread02 = new Thread(runnable03);
        thread.start();
        thread02.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread02.interrupt();

        while (thread.isAlive() || thread02.isAlive()){

        }
    }

}

class Runnable01 implements Runnable {

    static Lock lock1 = new ReentrantLock();
    static Lock lock2 = new ReentrantLock();

    int flag;

    public Runnable01(int flag) {
        this.flag = flag;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if (flag == 1) {
                try {
                    if (lock1.tryLock(800, TimeUnit.MILLISECONDS)) {
                        try {
                            System.out.println("线程1获取到了锁1");
                            int i1 = new Random().nextInt(1000);
                            System.out.println("线程1获取到了锁1,休眠:"+i1+"s");
                            Thread.sleep(i1);
                            if (lock2.tryLock(800, TimeUnit.MILLISECONDS)) {
                                try {
                                    System.out.println("线程1获取到了锁2");
                                    System.out.println("线程1成功获取到了两把锁");
                                    break;
                                } finally {
                                    lock2.unlock();
                                }
                            } else {
                                System.out.println("线程1获取锁2失败，已重试");
                            }
                        } finally {
                            lock1.unlock();
                            Thread.sleep(new Random().nextInt(1000));
                        }
                    } else {
                        System.out.println("线程1获取锁1失败，已重试");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (flag == 0) {
                try {
                    if (lock2.tryLock(3000, TimeUnit.MILLISECONDS)) {
                        try {
                            System.out.println("线程2获取到了锁2");
                            int i1 = new Random().nextInt(1000);
                            System.out.println("线程2获取到了锁2,休眠:"+i1+"s");
                            Thread.sleep(i1);
                            if (lock1.tryLock(800, TimeUnit.MILLISECONDS)) {
                                try {
                                    System.out.println("线程2获取到了锁1");
                                    System.out.println("线程2成功获取到了两把锁");
                                    break;
                                } finally {
                                    lock1.unlock();
                                }
                            } else {
                                System.out.println("线程2获取锁1失败，已重试");
                            }
                        } finally {
                            lock2.unlock();
                            Thread.sleep(new Random().nextInt(1000));
                        }
                    } else {
                        System.out.println("线程2获取锁2失败，已重试");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

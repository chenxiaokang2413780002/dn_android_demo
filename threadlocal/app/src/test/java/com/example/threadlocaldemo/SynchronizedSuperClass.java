package com.example.threadlocaldemo;

import org.junit.Test;

public class SynchronizedSuperClass {

    @Test
    public void test01(){
        TestClass testClass = new TestClass();
        testClass.method();
    }

}

class SuperClass{

    public synchronized void method(){
        System.out.println("我是父类的方法");
    }

}

class TestClass extends SuperClass{

    @Override
    public synchronized void method() {
        super.method();
        System.out.println("我是子类的方法");
    }
}



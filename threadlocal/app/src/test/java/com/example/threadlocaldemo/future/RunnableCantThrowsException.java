package com.example.threadlocaldemo.future;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class RunnableCantThrowsException {

    //在run方法中无法抛出checked Exception
    @Test
    public void test01(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    throw new Exception();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    //演示一个Future的使用方法
    @Test
    public void test02(){
        ExecutorService service = Executors.newFixedThreadPool(10);
        Future<Integer> future = service.submit(new CallableTask());
        try {
            System.out.println(future.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        service.shutdown();
    }

    //演示批量提交任务时，用List来批量获取结果
    @Test
    public void test03() throws InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(20);
        ArrayList<Future> futures = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            Future<Integer> future = service.submit(new CallableTask());
            futures.add(future);
        }
        Thread.sleep(5000);
        for (int i = 0; i < 20; i++) {
            Future<Integer> future = futures.get(i);
            try {
                Integer integer = future.get();
                System.out.println(integer);
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //演示get方法过程中抛出异常，for循环为了演示抛出Exception的时机：并不是说一产生异常就抛出，直到我们get执行时，才会抛出。
    @Test
    public void test04(){
        ExecutorService service = Executors.newFixedThreadPool(20);
        Future<Integer> future = service.submit(new CallableTask04());
        try {
            for (int i = 0; i < 5; i++) {
                System.out.println(i);
                Thread.sleep(500);
            }
            System.out.println(future.isDone());
            future.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            System.out.println("ExecutionException异常");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("InterruptedException异常");
        }
    }


    private final Ad DEFAULT_AD = new Ad("无网络时候的默认广告");
    private final ExecutorService service = Executors.newFixedThreadPool(2);

    class FetchAdTask implements Callable<Ad>{

        @Override
        public Ad call() throws Exception {
            try{
                Thread.sleep(3000);
            }catch (InterruptedException e){
                System.out.println("sleep期间被中断了");
                return new Ad("被中断时候的默认广告");
            }
            return new Ad("淘宝广告");
        }

    }

    public void printAd(){
        Future<Ad> f = service.submit(new FetchAdTask());
        Ad ad;

        try {
            ad = f.get(2000, TimeUnit.MILLISECONDS);
        } catch (ExecutionException e) {
            e.printStackTrace();
            ad = new Ad("异常时候的默认广告");
        } catch (InterruptedException e) {
            e.printStackTrace();
            ad = new Ad("中断时候的默认广告");
        } catch (TimeoutException e){
//            e.printStackTrace();
            ad = new Ad("异常时候的默认广告");

            System.out.println("超时了，未获取到广告");

            //cancel true 中断 false不会中断
            boolean cancel = f.cancel(true);
            System.out.println("cancel的结果" + cancel);
        }

        service.shutdown();
        System.out.println(ad);
    }

    //演示get的超时方法，需要注意超时后处理，调用future.cancel()。演示cancel传入true和false的区别，代表是否中断正在执行的任务。
    @Test
    public void test05(){

        printAd();

    }
}


class Ad{

    String name;

    public Ad(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return "Ad{" +
                "name='" + name + '\'' +
                '}';
    }
}

class CallableTask04 implements Callable<Integer>{

    @Override
    public Integer call() throws Exception {
        throw new IllegalArgumentException("Callable抛出异常");
    }
}

class CallableTask implements Callable<Integer>{

    @Override
    public Integer call() throws Exception {

        Thread.sleep(3000);

        return new Random().nextInt();
    }
}
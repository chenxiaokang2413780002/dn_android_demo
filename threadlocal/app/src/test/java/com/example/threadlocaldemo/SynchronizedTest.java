package com.example.threadlocaldemo;

import org.junit.Test;

public class SynchronizedTest {

    public Object lock = new Object();

    //代码块锁
    public Runnable runnable = new Runnable() {
        @Override
        public void run() {
            synchronized (lock) { //this或者自建对象
                System.out.println("我是对象锁的代码块形式，我叫：" + Thread.currentThread().getName());
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + ":运行结束");
            }
        }
    };

    //方法锁
    public Runnable runnable2 = new Runnable() {
        @Override
        public synchronized void run() {
            System.out.println("我是对象锁的方法修饰符形式，我叫：" + Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + ":运行结束");
        }
    };

    //静态方法加锁
//    class RunnableTest implements Runnable{
//        @Override
//        public void run() {
//            method();
//        }
//    }
//
//    public synchronized static void method(){
//        System.out.println("我是对象锁的静态方法修饰符形式，我叫：" + Thread.currentThread().getName());
//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println(Thread.currentThread().getName() + ":运行结束");
//    }

    //class对象锁
    public Runnable runnable5 = new Runnable() {
        @Override
        public void run() {
            synchronized (SynchronizedTest.class){
                System.out.println("我是对象锁的class对象修饰符形式，我叫：" + Thread.currentThread().getName());
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + ":运行结束");
            }
        }
    };

    @Test
    public void test01() {

        Thread t1 = new Thread(runnable5);
        Thread t2 = new Thread(runnable5);
        t1.start();
        t2.start();
        while (t1.isAlive() || t2.isAlive()) {

        }
        System.out.println("finished");
    }

}

class RunnableTest implements Runnable{

    @Override
    public void run() {
        method();
    }

    public static synchronized void method(){
        System.out.println("我是对象锁的静态方法修饰符形式，我叫：" + Thread.currentThread().getName());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + ":运行结束");
    }

}

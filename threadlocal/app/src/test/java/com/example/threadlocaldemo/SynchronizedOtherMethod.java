package com.example.threadlocaldemo;

import org.junit.Test;

public class SynchronizedOtherMethod {

    private synchronized void method(){
        System.out.println("这是method1");
        method2();
    }

    private synchronized void method2(){
        System.out.println("这是method2");
    }

    @Test
    public void test01(){
        method();
    }
}

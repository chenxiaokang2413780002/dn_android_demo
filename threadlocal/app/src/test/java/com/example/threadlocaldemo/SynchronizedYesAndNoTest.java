package com.example.threadlocaldemo;

import org.junit.Test;

public class SynchronizedYesAndNoTest {

    private Runnable runnable = new Runnable(){
        @Override
        public void run() {
            if(Thread.currentThread().getName().equals("Thread-0")){
                method();
            }else {
                method2();
            }
        }
    };

    public synchronized void method(){
        System.out.println("我是加锁的方法，我叫：" + Thread.currentThread().getName());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + ":运行结束");
    }

    public void method2(){
        System.out.println("我是没加锁的方法，我叫：" + Thread.currentThread().getName());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + ":运行结束");
    }

    @Test
    public void test01() {

        Thread t1 = new Thread(runnable);
        Thread t2 = new Thread(runnable);
        t1.start();
        t2.start();
        while (t1.isAlive() || t2.isAlive()) {

        }
        System.out.println("finished");
    }

}

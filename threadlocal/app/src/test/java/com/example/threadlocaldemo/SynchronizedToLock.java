package com.example.threadlocaldemo;

import org.junit.Test;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SynchronizedToLock {

    Lock lock = new ReentrantLock();

    public synchronized void method1(){
        System.out.println("我是Synchronized形式的锁");
    }

    public void method2(){
        lock.lock();

        try{
            System.out.println("我是lock形式的锁");
        }finally {
            lock.unlock();
        }
    }

    @Test
    public void test01(){
        method1();
        method2();
    }

}
